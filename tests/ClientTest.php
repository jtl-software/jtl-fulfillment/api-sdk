<?php
namespace Jtl\Fulfillment\Api\Sdk\Test;

use GuzzleHttp\ClientInterface;
use Jtl\Fulfillment\Api\Sdk\Client;
use PHPUnit\Framework\TestCase;
use GuzzleHttp\Client as HttpClient;
use Psr\SimpleCache\CacheInterface;

/**
 * Class ClientTest
 * @package Jtl\Fulfillment\Api\Sdk\Test
 */
class ClientTest extends TestCase
{
    public function testCanBeCreatedWithUserId(): void
    {
        $client = new Client(new HttpClient(), '1');
    
        $this->assertInstanceOf(
            Client::class,
            $client
        );
    
        $this->assertEquals('1', $client->getUserId());
    }
    
    public function testCanHoldHttpClientInterface(): void
    {
        $client = new Client(new HttpClient(), '1');
        
        $http = new HttpClient();
        $client->setHttp($http);
        
        $this->assertInstanceOf(ClientInterface::class, $client->getHttp());
    }
    
    public function testCanHoldCacheInterface(): void
    {
        $client = new Client(new HttpClient(), '1');
        
        $this->assertInstanceOf(CacheInterface::class, $client->getCache());
    }
}
