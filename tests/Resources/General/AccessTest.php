<?php
namespace Jtl\Fulfillment\Api\Sdk\Test\Resources\General;

use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Client as HttpClient;
use Izzle\Model\PropertyInfo;
use Jtl\Fulfillment\Api\Sdk\Client;
use Jtl\Fulfillment\Api\Sdk\Models\General\Access;
use Jtl\Fulfillment\Api\Sdk\Models\General\Access\Token;
use Jtl\Fulfillment\Api\Sdk\Models\Pagination;
use Jtl\Fulfillment\Api\Sdk\Resources\General\AccessResource;
use Jtl\Fulfillment\Api\Sdk\Resources\General\ProductResource;
use Jtl\Fulfillment\Api\Sdk\Test\Resources\AbstractResourceTest;

/**
 * Class AccessTest
 * @package Jtl\Fulfillment\Api\Sdk\Test\Resources\General
 */
class AccessTest extends AbstractResourceTest
{
    public function testCanQueryAll(): void
    {
        $this->canQueryAll(
            __DIR__ . '/MockData/access_all.json',
            AccessResource::class,
            Access::class,
            'all',
            static function (Pagination $result) {
                self::assertEquals(1, $result->getPage()->getTotal());
            }
        );
    }
    
    public function testCanLock(): void
    {
        $mock = new MockHandler([
            new Response(201, []),
            new Response(401),
            new Response(404)
        ]);
    
        $handler = HandlerStack::create($mock);
        $client = new Client(new HttpClient(['handler' => $handler]));
    
        /** @var AccessResource $resource */
        $resource = new AccessResource($client);
        self::assertTrue($resource->lock());
    
        $this->expectException(ClientException::class);
        $resource->lock();
    
        $this->expectException(ClientException::class);
        $resource->lock();
    }
    
    public function testCanUnLock(): void
    {
        $mock = new MockHandler([
            new Response(204, []),
            new Response(401),
            new Response(404)
        ]);
        
        $handler = HandlerStack::create($mock);
        $client = new Client(new HttpClient(['handler' => $handler]));
        
        /** @var AccessResource $resource */
        $resource = new AccessResource($client);
        self::assertTrue($resource->unlock());
        
        $this->expectException(ClientException::class);
        $resource->unlock();
        
        $this->expectException(ClientException::class);
        $resource->unlock();
    }
    
    public function testCanQueryAllTokens(): void
    {
        $this->canQueryAll(
            __DIR__ . '/MockData/access_tokens_all.json',
            AccessResource::class,
            Token::class,
            'allTokens'
        );
    }
    
    public function testCanFindOneToken(): void
    {
        $this->canFindOneBy(
            __DIR__ . '/MockData/access_tokens_one.json',
            AccessResource::class,
            Token::class,
            'findToken',
            '1234'
        );
    }
    
    public function testCanCreateOneToken(): void
    {
        $mock = new MockHandler([
            new Response(201, [], file_get_contents(__DIR__ . '/MockData/access_tokens_one.json')),
            new Response(403),
            new Response(404)
        ]);

        $handler = HandlerStack::create($mock);
        $client = new Client(new HttpClient(['handler' => $handler]));

        $resource = new AccessResource($client);
        $token = $resource->createToken(new Token(['applicationId' => 'XYZ', 'name' => 'foobar']));

        self::assertInstanceOf(Token::class, $token);
        self::assertEquals('foo', $token->getToken());
        self::assertEquals('1234', $token->getTokenId());

        $this->expectException(ClientException::class);
        $resource->createToken(new Token(['applicationId' => 'XYZ', 'name' => 'foobar']));

        $this->expectException(ClientException::class);
        $resource->createToken(new Token(['applicationId' => 'XYZ', 'name' => 'foobar']));
    }
    
    public function testCanRemoveOneToken(): void
    {
        $this->canRemove(
            __DIR__ . '/MockData/access_tokens_one.json',
            AccessResource::class,
            Token::class,
            null,
            'removeToken'
        );
    }
}
