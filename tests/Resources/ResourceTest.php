<?php
namespace Jtl\Fulfillment\Api\Sdk\Test\Resources;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use Jtl\Fulfillment\Api\Sdk\Cache\ArrayCache;
use Jtl\Fulfillment\Api\Sdk\Models\DataModel;
use Jtl\Fulfillment\Api\Sdk\Models\Merchant\Product\Product;
use Jtl\Fulfillment\Api\Sdk\Models\Pagination;
use Jtl\Fulfillment\Api\Sdk\Models\Query;
use Jtl\Fulfillment\Api\Sdk\Resources\Merchant\ProductResource;
use Jtl\Fulfillment\Api\Sdk\Resources\Resource;
use PHPUnit\Framework\TestCase;
use GuzzleHttp\Psr7\Response;
use Jtl\Fulfillment\Api\Sdk\Client;
use GuzzleHttp\Client as HttpClient;

/**
 * Class ResourceTest
 * @package Jtl\Fulfillment\Api\Sdk\Test\Resources
 */
class ResourceTest extends TestCase
{
    public function testCanCacheFind(): void
    {
        $json = file_get_contents(__DIR__ . '/Merchant/MockData/product_one.json');
    
        $mock = new MockHandler([
            new Response(200, [], $json),
            new Response(404),
        ]);
    
        $handler = HandlerStack::create($mock);
        $client = (new Client(new HttpClient(['handler' => $handler]), '0'))->setCache(new ArrayCache());
    
        $resource = new ProductResource($client);
        $product = $resource->find('MERC01PRDCT');
    
        $this->assertInstanceOf(Product::class, $product);
    
        // From Cache
        $cachedProduct = $resource->find('MERC01PRDCT');
        $this->assertInstanceOf(Product::class, $cachedProduct);
        $this->assertEquals($product, $cachedProduct);
    }
    
    public function testCanCacheFindAll(): void
    {
        $json = file_get_contents(__DIR__ . '/Merchant/MockData/product_all.json');
    
        $mock = new MockHandler([
            new Response(200, [], $json),
            new Response(404),
        ]);
    
        $handler = HandlerStack::create($mock);
        $client = (new Client(new HttpClient(['handler' => $handler]), '0'))->setCache(new ArrayCache());
    
        $resource = new ProductResource($client);
        $pagination = $resource->all(new Query());
    
        $this->assertInstanceOf(Pagination::class, $pagination);
        $this->assertCount(1, $pagination->getItems());
        
        /** @var Product $item */
        foreach ($pagination->getItems() as $item) {
            $this->assertInstanceOf(Product::class, $item);
            $this->assertEquals('MERC01PRDCT', $item->getJfsku());
        }
    
        // From Cache
        $cachedPagination = $resource->all(new Query());
        $this->assertInstanceOf(Pagination::class, $cachedPagination);
        $this->assertCount(1, $cachedPagination->getItems());
    
        /** @var Product $item */
        foreach ($cachedPagination->getItems() as $item) {
            $this->assertInstanceOf(Product::class, $item);
            $this->assertEquals('MERC01PRDCT', $item->getJfsku());
        }
    }
    
    public function testCanCacheOnCreate(): void
    {
        $json = file_get_contents(__DIR__ . '/Merchant/MockData/product_one.json');
    
        $mock = new MockHandler([
            new Response(201, [], $json),
            new Response(404),
        ]);
    
        $handler = HandlerStack::create($mock);
        $client = (new Client(new HttpClient(['handler' => $handler]), '0'))->setCache(new ArrayCache(300, false));
    
        /** @var Product $model */
        $model = (new Product(json_decode($json, true)))->setJfsku('')->setModificationInfo(null);
        
        define('foo', 'bar');
        $resource = new ProductResource($client);
        $resource->save($model);
        
        // From Cache
        $cachedProduct = $resource->find($model->getJfsku());
        
        $this->assertInstanceOf(Product::class, $cachedProduct);
        $this->assertEquals('MERC01PRDCT', $cachedProduct->getJfsku());
    }
    
    public function testCanCacheOnUpdate(): void
    {
        $json = file_get_contents(__DIR__ . '/Merchant/MockData/product_one.json');
        
        $mock = new MockHandler([
            new Response(204),
            new Response(404),
        ]);
        
        $handler = HandlerStack::create($mock);
        $client = (new Client(new HttpClient(['handler' => $handler]), '0'))->setCache(new ArrayCache());
        
        $model = new Product(json_decode($json, true));
        
        $resource = new ProductResource($client);
        $resource->save($model);
    
        // From Cache
        $cachedProduct = $resource->find($model->getJfsku());
    
        $this->assertInstanceOf(Product::class, $cachedProduct);
        $this->assertEquals('MERC01PRDCT', $cachedProduct->getJfsku());
    }
    
    public function testCanRemoveCache(): void
    {
        $json = file_get_contents(__DIR__ . '/Merchant/MockData/product_one.json');
        $jsonAll = file_get_contents(__DIR__ . '/Merchant/MockData/product_all.json');
        $jsonPagination = file_get_contents(__DIR__ . '/Merchant/MockData/pagination_empty.json');
    
        $mock = new MockHandler([
            new Response(200, [], $json),
            new Response(200, [], $jsonAll),
            new Response(204),
            new Response(404),
            new Response(200, [], $jsonPagination),
        ]);
    
        $handler = HandlerStack::create($mock);
        $client = (new Client(new HttpClient(['handler' => $handler]), '0'))->setCache(new ArrayCache());
    
        $resource = new ProductResource($client);
        $product = $resource->find('MERC01PRDCT');
    
        $this->assertInstanceOf(Product::class, $product);
        $this->assertEquals('MERC01PRDCT', $product->getJfsku());
    
        $pagination = $resource->all(new Query());
    
        $this->assertInstanceOf(Pagination::class, $pagination);
        $this->assertCount(1, $pagination->getItems());
    
        // From Cache
        $cachedProduct = $resource->find('MERC01PRDCT');
    
        $this->assertInstanceOf(Product::class, $cachedProduct);
        $this->assertEquals('MERC01PRDCT', $cachedProduct->getJfsku());
    
        $resource->remove($cachedProduct);
    
        // Must be deleted
        $cachedProduct = $resource->find('MERC01PRDCT');
    
        $this->assertNull($cachedProduct);
    
        $pagination = $resource->all(new Query());
        $this->assertInstanceOf(Pagination::class, $pagination);
        $this->assertCount(0, $pagination->getItems());
    }
    
    public function testCanRemoveEmpty(): void
    {
        $json = file_get_contents(__DIR__ . '/General/MockData/product_one_empty.json');
        $product = new Product(json_decode($json, true));
    
        $data = Resource::removeEmpty($product, $product->toArray());
        
        $this->assertCount(4, $data);
        $this->assertArrayNotHasKey('productGroup', $data);
        $this->assertArrayNotHasKey('manufacturer', $data);
        $this->assertArrayNotHasKey('dimensions', $data);
        $this->assertArrayNotHasKey('bundles', $data);
        
        foreach ([
                'name',
                'merchantSku',
                'originCountry',
                'specifications'
             ] as $key) {
            $this->assertArrayHasKey($key, $data);
        }
    }

    public function testCanRemoveEmptyAndPreserve(): void
    {
        $json = file_get_contents(__DIR__ . '/General/MockData/product_one_empty.json');
        $product = new Product(json_decode($json, true));

        $product->setPreservables(['bundles', 'pictures', 'attributes']);

        $data = Resource::removeEmpty($product, $product->toArray());

        $this->assertCount(7, $data);

        $this->assertArrayNotHasKey('productGroup', $data);
        $this->assertArrayNotHasKey('manufacturer', $data);
        $this->assertArrayNotHasKey('stock', $data);
        $this->assertArrayNotHasKey('dimensions', $data);

        foreach ([
                     'name',
                     'merchantSku',
                     'originCountry',
                     'specifications',
                     'pictures',
                     'attributes',
                     'bundles'
                 ] as $key) {
            $this->assertArrayHasKey($key, $data);
        }
    }
}
