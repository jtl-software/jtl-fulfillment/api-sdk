<?php
namespace Jtl\Fulfillment\Api\Sdk\Test\Resources\Fulfiller;

use GuzzleHttp\Exception\ClientException;
use Izzle\Model\PropertyInfo;
use Jtl\Fulfillment\Api\Sdk\Models\Fulfiller\Inbound\Inbound;
use Jtl\Fulfillment\Api\Sdk\Models\Fulfiller\Inbound\InboundShippingNotification;
use Jtl\Fulfillment\Api\Sdk\Models\Fulfiller\Inbound\IncomingGood;
use Jtl\Fulfillment\Api\Sdk\Models\Fulfiller\Inbound\IncomingGoodItem;
use Jtl\Fulfillment\Api\Sdk\Models\Pagination;
use Jtl\Fulfillment\Api\Sdk\Resources\Fulfiller\InboundResource;
use Jtl\Fulfillment\Api\Sdk\Test\Resources\AbstractResourceTest;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use Jtl\Fulfillment\Api\Sdk\Client;
use GuzzleHttp\Client as HttpClient;

/**
 * Class InboundTest
 * @package Jtl\Fulfillment\Api\Sdk\Test\Resources\Fulfiller
 */
class InboundTest extends AbstractResourceTest
{
    public function testCanQueryAll(): void
    {
        $this->canQueryAll(__DIR__ . '/MockData/inbound_all.json', InboundResource::class, Inbound::class);
    }
    
    public function testCanFindOne(): void
    {
        $this->canFindOne(
            __DIR__ . '/MockData/inbound_one.json',
            InboundResource::class,
            Inbound::class,
            'MERC03INBND01',
            new PropertyInfo('inboundId')
        );
    }
    
    public function testFindShippingNotifications(): void
    {
        $mock = new MockHandler([
            new Response(200, [], file_get_contents(__DIR__ . '/MockData/inbound_notification_all.json')),
            new Response(403),
            new Response(404)
        ]);
        
        $handler = HandlerStack::create($mock);
        $client = new Client(new HttpClient(['handler' => $handler]));
        
        $resource = new InboundResource($client);
        
        $pagination = $resource->findShippingNotifications('My_Identifier001');
        
        $this->assertInstanceOf(Pagination::class, $pagination);
        $this->assertEquals(1, $pagination->getPage()->getTotal());
        $this->assertInstanceOf(InboundShippingNotification::class, $pagination->getItems()[0]);
        
        $this->expectException(ClientException::class);
        $resource->findShippingNotifications('My_Identifier001');
        
        $this->assertEmpty($resource->findShippingNotifications('My_Identifier001'));
    }
    
    public function testCanFindShippingNotificationById(): void
    {
        $mock = new MockHandler([
            new Response(200, [], file_get_contents(__DIR__ . '/MockData/inbound_notification_one.json')),
            new Response(403),
            new Response(404)
        ]);
        
        $handler = HandlerStack::create($mock);
        $client = new Client(new HttpClient(['handler' => $handler]));
        
        $resource = new InboundResource($client);
        
        $notification = $resource->findShippingNotificationById('My_Identifier001', 'Foobar');
        
        $this->assertInstanceOf(InboundShippingNotification::class, $notification);
        
        $this->expectException(ClientException::class);
        $resource->findShippingNotificationById('My_Identifier001', 'Foobar');
        
        $this->assertEmpty($resource->findShippingNotificationById('My_Identifier001', 'Foobar'));
    }
    
    public function testItemCanArrive(): void
    {
        $mock = new MockHandler([
            new Response(201),
            new Response(403),
            new Response(404)
        ]);
        
        $handler = HandlerStack::create($mock);
        $client = new Client(new HttpClient(['handler' => $handler]));
        
        $resource = new InboundResource($client);
        
        $this->assertTrue($resource->itemArrived('AAA', new IncomingGoodItem()));
    
        $this->expectException(ClientException::class);
        $resource->itemArrived('AAA', new IncomingGoodItem());
    
        $this->assertFalse($resource->itemArrived('AAA', new IncomingGoodItem()));
    }
    
    public function testItemsCanArrive(): void
    {
        $mock = new MockHandler([
            new Response(201),
            new Response(403),
            new Response(404)
        ]);
        
        $handler = HandlerStack::create($mock);
        $client = new Client(new HttpClient(['handler' => $handler]));
        
        $resource = new InboundResource($client);
        
        $this->assertTrue($resource->itemsArrived('AAA', new IncomingGood()));
        
        $this->expectException(ClientException::class);
        $resource->itemsArrived('AAA', new IncomingGood());
        
        $this->assertFalse($resource->itemsArrived('AAA', new IncomingGood()));
    }
    
    public function testInboundCanBeClosed(): void
    {
        $mock = new MockHandler([
            new Response(200),
            new Response(403),
            new Response(404)
        ]);
        
        $handler = HandlerStack::create($mock);
        $client = new Client(new HttpClient(['handler' => $handler]));
        
        $resource = new InboundResource($client);
        
        $this->assertTrue($resource->close('AAA'));
    
        $this->expectException(ClientException::class);
        $resource->close('AAA');
    
        $this->assertFalse($resource->close('AAA'));
    }
}
