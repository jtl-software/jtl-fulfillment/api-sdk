<?php
namespace Jtl\Fulfillment\Api\Sdk\Test\Resources\Fulfiller;

use Izzle\Model\PropertyInfo;
use Jtl\Fulfillment\Api\Sdk\Models\General\ShippingMethod;
use Jtl\Fulfillment\Api\Sdk\Resources\Fulfiller\ShippingMethodResource;
use Jtl\Fulfillment\Api\Sdk\Test\Resources\AbstractResourceTest;

/**
 * Class ShippingMethodTest
 * @package Jtl\Fulfillment\Api\Sdk\Test\Resources\Fulfiller
 */
class ShippingMethodTest extends AbstractResourceTest
{
    public function testCanQueryAll(): void
    {
        $this->canQueryAll(
            __DIR__ . '/MockData/shipping_method_call.json',
            ShippingMethodResource::class,
            ShippingMethod::class
        );
    }
    
    public function testCanCreate(): void
    {
        $this->canCreate(
            __DIR__ . '/MockData/shipping_method_one.json',
            ShippingMethodResource::class,
            ShippingMethod::class,
            'XYZ',
            new PropertyInfo('shippingMethodId')
        );
    }
    
    public function testCanRemove(): void
    {
        $this->canRemove(
            __DIR__ . '/MockData/shipping_method_one.json',
            ShippingMethodResource::class,
            ShippingMethod::class
        );
    }
    
    public function testCanFindOne(): void
    {
        $this->canFindOne(
            __DIR__ . '/MockData/shipping_method_one.json',
            ShippingMethodResource::class,
            ShippingMethod::class,
            'XYZ',
            new PropertyInfo('shippingMethodId')
        );
    }
}
