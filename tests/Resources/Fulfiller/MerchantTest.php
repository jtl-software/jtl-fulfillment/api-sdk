<?php
namespace Jtl\Fulfillment\Api\Sdk\Test\Resources\Fulfiller;

use Izzle\Model\PropertyInfo;
use Jtl\Fulfillment\Api\Sdk\Models\Fulfiller\Merchant\Merchant;
use Jtl\Fulfillment\Api\Sdk\Resources\Fulfiller\MerchantResource;
use Jtl\Fulfillment\Api\Sdk\Test\Resources\AbstractResourceTest;

/**
 * Class MerchantTest
 * @package Jtl\Fulfillment\Api\Sdk\Test\Resources\Fulfiller
 */
class MerchantTest extends AbstractResourceTest
{
    public function testCanFindOneById(): void
    {
        $this->canFindOneBy(
            __DIR__ . '/MockData/merchant_one.json',
            MerchantResource::class,
            Merchant::class,
            'findById',
            'ABCD',
            new PropertyInfo('userId')
        );
    }
}
