<?php
namespace Jtl\Fulfillment\Api\Sdk\Test\Resources\Fulfiller;

use GuzzleHttp\Exception\ClientException;
use Izzle\Model\PropertyInfo;
use Jtl\Fulfillment\Api\Sdk\Models\Fulfiller\Authorization\Authorization;
use Jtl\Fulfillment\Api\Sdk\Resources\Fulfiller\AuthorizationResource;
use Jtl\Fulfillment\Api\Sdk\Test\Resources\AbstractResourceTest;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use Jtl\Fulfillment\Api\Sdk\Client;
use GuzzleHttp\Client as HttpClient;

/**
 * Class AuthorizationTest
 * @package Jtl\Fulfillment\Api\Sdk\Test\Resources\Fulfiller
 */
class AuthorizationTest extends AbstractResourceTest
{
    public function testCanQueryAll(): void
    {
        $this->canQueryAll(__DIR__ . '/MockData/warehouse_all.json', AuthorizationResource::class, Authorization::class);
    }
    
    public function testCanFindOneByMerchanId(): void
    {
        $this->canFindOneBy(
            __DIR__ . '/MockData/authorization_one.json',
            AuthorizationResource::class,
            Authorization::class,
            'findByMerchantId',
            'HJ01',
            new PropertyInfo('merchantId')
        );
    }
    
    public function testCanAuthorizeMerchant(): void
    {
        $mock = new MockHandler([
            new Response(200, [], file_get_contents(__DIR__ . '/MockData/authorization_one.json')),
            new Response(403),
            new Response(404)
        ]);
    
        $handler = HandlerStack::create($mock);
        $client = new Client(new HttpClient(['handler' => $handler]));
    
        $resource = new AuthorizationResource($client);
    
        $model = $resource->authorizeMerchant('HJ01', 'FULF04XX-12345-0001');
    
        $this->assertInstanceOf(Authorization::class, $model);
        $this->assertEquals('HJ01', $model->getMerchantId());
    
        $this->expectException(ClientException::class);
        $resource->authorizeMerchant('HJ01', 'FULF04XX-12345-0001');
    
        $this->assertEmpty($resource->authorizeMerchant('HJ01', 'FULF04XX-12345-0001'));
    }
    
    public function testCanAssignShippingMethod(): void
    {
        $mock = new MockHandler([
            new Response(200, [], file_get_contents(__DIR__ . '/MockData/authorization_one.json')),
            new Response(403),
            new Response(404)
        ]);
    
        $handler = HandlerStack::create($mock);
        $client = new Client(new HttpClient(['handler' => $handler]));
    
        $resource = new AuthorizationResource($client);
    
        $model = $resource->assignShippingMethod('HJ01', 'FULF04XX-12345-0001', 'ABC');
    
        $this->assertInstanceOf(Authorization::class, $model);
        $this->assertEquals('HJ01', $model->getMerchantId());
    
        $this->expectException(ClientException::class);
        $resource->assignShippingMethod('HJ01', 'FULF04XX-12345-0001', 'ABC');
    
        $this->assertEmpty($resource->assignShippingMethod('HJ01', 'FULF04XX-12345-0001', 'ABC'));
    }
    
    public function testCanRemoveAuthorization(): void
    {
        $mock = new MockHandler([
            new Response(200),
            new Response(403),
            new Response(404)
        ]);
    
        $handler = HandlerStack::create($mock);
        $client = new Client(new HttpClient(['handler' => $handler]));
    
        $resource = new AuthorizationResource($client);
        
        $this->assertTrue($resource->removeAuthorization('HJ01', 'FULF04XX-12345-0001'));
    
        $this->expectException(ClientException::class);
        $resource->removeAuthorization('HJ01', 'FULF04XX-12345-0001');
    
        $this->assertFalse($resource->removeAuthorization('HJ01', 'FULF04XX-12345-0001'));
    }
    
    public function testCanRemoveShippingMethod(): void
    {
        $mock = new MockHandler([
            new Response(200),
            new Response(403),
            new Response(404)
        ]);
        
        $handler = HandlerStack::create($mock);
        $client = new Client(new HttpClient(['handler' => $handler]));
        
        $resource = new AuthorizationResource($client);
        
        $this->assertTrue($resource->removeShippingMethod('HJ01', 'FULF04XX-12345-0001', 'ABC'));
    
        $this->expectException(ClientException::class);
        $resource->removeShippingMethod('HJ01', 'FULF04XX-12345-0001', 'ABC');
    
        $this->assertFalse($resource->removeShippingMethod('HJ01', 'FULF04XX-12345-0001', 'ABC'));
    }
}
