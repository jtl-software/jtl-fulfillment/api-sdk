<?php
namespace Jtl\Fulfillment\Api\Sdk\Test\Resources\Fulfiller;

use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use Izzle\Model\PropertyInfo;
use GuzzleHttp\Client as HttpClient;
use Jtl\Fulfillment\Api\Sdk\Client;
use Jtl\Fulfillment\Api\Sdk\Models\Fulfiller\Outbound\Outbound;
use Jtl\Fulfillment\Api\Sdk\Models\Fulfiller\Outbound\OutboundShippingNotification;
use Jtl\Fulfillment\Api\Sdk\Models\Fulfiller\Returns\FulfillerReturn;
use Jtl\Fulfillment\Api\Sdk\Models\General\Outbound\OutboundShippingNotification as GeneralOutboundShippingNotification;
use Jtl\Fulfillment\Api\Sdk\Models\General\Attachment;
use Jtl\Fulfillment\Api\Sdk\Models\Pagination;
use Jtl\Fulfillment\Api\Sdk\Resources\Fulfiller\OutboundResource;
use Jtl\Fulfillment\Api\Sdk\Test\Resources\AbstractResourceTest;

/**
 * Class OutboundTest
 * @package Jtl\Fulfillment\Api\Sdk\Test\Resources\Fulfiller
 */
class OutboundTest extends AbstractResourceTest
{
    public function testCanQueryAll(): void
    {
        $this->canQueryAll(
            __DIR__ . '/MockData/outbound_all.json',
            OutboundResource::class,
            Outbound::class,
            'all',
            function (Pagination $pagination) {
                /** @var Outbound $outbound */
                $outbound = $pagination->getItems()[0];
                $this->assertEquals('MERC02AXEV', $outbound->getOutboundId());
                $this->assertCount(1, $outbound->getRelatedReturns());

                $relatedReturn = $outbound->getRelatedReturns()[0];
                $this->assertInstanceOf(FulfillerReturn::class, $relatedReturn);
                $this->assertEquals('FCTH05E', $relatedReturn->getReturnId());
            }
        );
    }
    
    public function testCanFindOne(): void
    {
        $this->canFindOne(
            __DIR__ . '/MockData/outbound_one.json',
            OutboundResource::class,
            Outbound::class,
            'MERC02AXEV',
            new PropertyInfo('outboundId')
        );
    }
    
    public function testCanFindShippingNotifications(): void
    {
        $mock = new MockHandler([
            new Response(200, [], file_get_contents(__DIR__ . '/MockData/outbound_notification_all.json')),
            new Response(403),
            new Response(404)
        ]);
    
        $handler = HandlerStack::create($mock);
        $client = new Client(new HttpClient(['handler' => $handler]));
    
        $resource = new OutboundResource($client);
    
        $pagination = $resource->findShippingNotifications('MERC02AXEV');
    
        $this->assertInstanceOf(Pagination::class, $pagination);
        $this->assertEquals(1, $pagination->getPage()->getTotal());
        $this->assertInstanceOf(OutboundShippingNotification::class, $pagination->getItems()[0]);
    
        $this->expectException(ClientException::class);
        $resource->findShippingNotifications('MERC02AXEV');
    
        $this->assertEmpty($resource->findShippingNotifications('MERC02AXEV'));
    }
    
    public function testCanQueryAllChanges(): void
    {
        $this->canQueryUpdates(
            __DIR__ . '/MockData/outbound_changes_all.json',
            OutboundResource::class,
            Outbound::class,
            'findChangesByTimeFrame'
        );
    }
    
    public function testCanFindShippingNotificationById(): void
    {
        $mock = new MockHandler([
            new Response(200, [], file_get_contents(__DIR__ . '/MockData/outbound_notification_one.json')),
            new Response(403),
            new Response(404)
        ]);
        
        $handler = HandlerStack::create($mock);
        $client = new Client(new HttpClient(['handler' => $handler]));
        
        $resource = new OutboundResource($client);
        
        $notification = $resource->findShippingNotificationById('MERC02AXEV', 'Foobar');
        
        $this->assertInstanceOf(OutboundShippingNotification::class, $notification);
    
        $this->expectException(ClientException::class);
        $resource->findShippingNotificationById('MERC02AXEV', 'Foobar');
    
        $this->assertEmpty($resource->findShippingNotificationById('MERC02AXEV', 'Foobar'));
    }
    
    public function testCanFindAttachment(): void
    {
        $mock = new MockHandler([
            new Response(200, [], file_get_contents(__DIR__ . '/MockData/outbound_attachment_one.json')),
            new Response(403),
            new Response(404)
        ]);
        
        $handler = HandlerStack::create($mock);
        $client = new Client(new HttpClient(['handler' => $handler]));
        
        $resource = new OutboundResource($client);
        
        $notification = $resource->findAttachment('MERC02AXEV', 'Foobar');
        
        $this->assertInstanceOf(Attachment::class, $notification);
    
        $this->expectException(ClientException::class);
        $resource->findAttachment('MERC02AXEV', 'Foobar');
    
        $this->assertEmpty($resource->findAttachment('MERC02AXEV', 'Foobar'));
    }
    
    public function testStatusCanBeUpdated(): void
    {
        $mock = new MockHandler([
            new Response(204),
            new Response(403),
            new Response(404)
        ]);
    
        $handler = HandlerStack::create($mock);
        $client = new Client(new HttpClient(['handler' => $handler]));
    
        $resource = new OutboundResource($client);
    
        $this->assertTrue($resource->updateStatus('MERC02AXEV', 'Foobar'));
    
        $this->expectException(ClientException::class);
        $resource->updateStatus('MERC02AXEV', 'Foobar');
    
        $this->assertfalse($resource->updateStatus('MERC02AXEV', 'Foobar'));
    }
    
    public function testWarehouseCanBeChanged(): void
    {
        $mock = new MockHandler([
            new Response(204),
            new Response(403),
            new Response(404)
        ]);
        
        $handler = HandlerStack::create($mock);
        $client = new Client(new HttpClient(['handler' => $handler]));
        
        $resource = new OutboundResource($client);
        
        $this->assertTrue($resource->changeWarehouse('MERC02AXEV', 'Foobar'));
    
        $this->expectException(ClientException::class);
        $resource->changeWarehouse('MERC02AXEV', 'Foobar');
    
        $this->assertfalse($resource->changeWarehouse('MERC02AXEV', 'Foobar'));
    }
    
    public function testCanDeclareAsShipped(): void
    {
        $mock = new MockHandler([
            new Response(201),
            new Response(403),
            new Response(404)
        ]);
    
        $handler = HandlerStack::create($mock);
        $client = new Client(new HttpClient(['handler' => $handler]));
    
        $resource = new OutboundResource($client);
        $this->assertTrue($resource->declareShipped('MERC02AXEV', new GeneralOutboundShippingNotification()));
    
        $this->expectException(ClientException::class);
        $resource->declareShipped('MERC02AXEV', new GeneralOutboundShippingNotification());
    
        $this->assertFalse($resource->declareShipped('MERC02AXEV', new GeneralOutboundShippingNotification()));
    }
}
