<?php
namespace Jtl\Fulfillment\Api\Sdk\Test\Resources\Merchant;

use GuzzleHttp\Exception\ClientException;
use Izzle\Model\PropertyInfo;
use Jtl\Fulfillment\Api\Sdk\Models\Merchant\Inbound\InboundShippingNotification;
use Jtl\Fulfillment\Api\Sdk\Models\Merchant\Inbound\Inbound;
use Jtl\Fulfillment\Api\Sdk\Models\Pagination;
use Jtl\Fulfillment\Api\Sdk\Resources\Merchant\InboundResource;
use Jtl\Fulfillment\Api\Sdk\Test\Resources\AbstractResourceTest;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use Jtl\Fulfillment\Api\Sdk\Client;
use GuzzleHttp\Client as HttpClient;

/**
 * Class InboundTest
 * @package Jtl\Fulfillment\Api\Sdk\Test\Resources\Merchant
 */
class InboundTest extends AbstractResourceTest
{
    public function testCanQueryAll(): void
    {
        $this->canQueryAll(__DIR__ . '/MockData/inbound_all.json', InboundResource::class, Inbound::class);
    }
    
    public function testCanFindOne(): void
    {
        $this->canFindOne(
            __DIR__ . '/MockData/inbound_one.json',
            InboundResource::class,
            Inbound::class,
            'MERC03INBND01',
            new PropertyInfo('inboundId')
        );
    }
    
    public function testCanUpdate(): void
    {
        $this->canUpdate(
            __DIR__ . '/MockData/inbound_one.json',
            InboundResource::class,
            Inbound::class,
            'MERC03INBND01',
            new PropertyInfo('inboundId')
        );
    }
    
    public function testCanCreate(): void
    {
        $this->canCreate(
            __DIR__ . '/MockData/inbound_one.json',
            InboundResource::class,
            Inbound::class,
            'MERC03INBND01',
            new PropertyInfo('inboundId')
        );
    }
    
    public function testCanBeClosed(): void
    {
        $mock = new MockHandler([
            new Response(200),
            new Response(403),
            new Response(404)
        ]);
    
        $handler = HandlerStack::create($mock);
        $client = new Client(new HttpClient(['handler' => $handler]));
    
        $resource = new InboundResource($client);
    
        $this->assertTrue($resource->close('MERC03INBND01'));
    
        $this->expectException(ClientException::class);
        $resource->close('MERC03INBND01');
    
        $this->assertFalse($resource->close('MERC03INBND01'));
    }
    
    public function testFindShippingNotifications(): void
    {
        $mock = new MockHandler([
            new Response(200, [], file_get_contents(__DIR__ . '/MockData/inbound_notification_all.json')),
            new Response(403),
            new Response(404)
        ]);
        
        $handler = HandlerStack::create($mock);
        $client = new Client(new HttpClient(['handler' => $handler]));
        
        $resource = new InboundResource($client);
    
        $pagination = $resource->findShippingNotifications('MERC03INBND01');
    
        $this->assertInstanceOf(Pagination::class, $pagination);
        $this->assertEquals(1, $pagination->getPage()->getTotal());
        $this->assertInstanceOf(InboundShippingNotification::class, $pagination->getItems()[0]);
        
        $this->expectException(ClientException::class);
        $resource->findShippingNotifications('MERC03INBND01');
        
        $this->assertEmpty($resource->findShippingNotifications('MERC03INBND01'));
    }
    
    public function testCanCreateShippingNotification(): void
    {
        $json = file_get_contents(__DIR__ . '/MockData/inbound_notification_one.json');
        
        $mock = new MockHandler([
            new Response(201, [], $json),
            new Response(403),
            new Response(404)
        ]);
    
        $handler = HandlerStack::create($mock);
        $client = new Client(new HttpClient(['handler' => $handler]));
    
        /** @var InboundResource $resource */
        $resource = new InboundResource($client);
    
        /** @var InboundShippingNotification $model */
        $model = new InboundShippingNotification(json_decode($json, true));
        $model->setMerchantShippingNotificationNumber('foobar123');
    
        $this->assertTrue($resource->createShippingNotification($model));
    
        $this->assertEquals('foobar123', $model->getMerchantShippingNotificationNumber());
    
        $this->expectException(ClientException::class);
        $resource->createShippingNotification($model);
    
        $this->assertEmpty($resource->createShippingNotification($model));
    }
    
    public function testCanFindShippingNotificationById(): void
    {
        $mock = new MockHandler([
            new Response(200, [], file_get_contents(__DIR__ . '/MockData/inbound_notification_one.json')),
            new Response(403),
            new Response(404)
        ]);
        
        $handler = HandlerStack::create($mock);
        $client = new Client(new HttpClient(['handler' => $handler]));
        
        $resource = new InboundResource($client);
        
        $notification = $resource->findShippingNotificationById('MERC03INBND01', 'Foobar');
        
        $this->assertInstanceOf(InboundShippingNotification::class, $notification);
        
        $this->expectException(ClientException::class);
        $resource->findShippingNotificationById('MERC03INBND01', 'Foobar');
        
        $this->assertEmpty($resource->findShippingNotificationById('MERC03INBND01', 'Foobar'));
    }
    
    public function testCanUpdateShippingNotification(): void
    {
        $json = file_get_contents(__DIR__ . '/MockData/inbound_notification_one.json');
        $client = $this->prepareHttpHandler(null, 204);
        
        $resource = new InboundResource($client);
        
        $model = new InboundShippingNotification(json_decode($json, true));
        $result = $resource->updateShippingNotification($model);
        
        $this->assertTrue($result);
        $this->assertEquals('MERC07XY', $model->getInboundShippingNotificationId());
        
        $this->expectException(ClientException::class);
        $resource->updateShippingNotification($model);
        
        $this->expectException(ClientException::class);
        $resource->updateShippingNotification($model);
    }
    
    public function testCanRemoveShippingNotification(): void
    {
        $mock = new MockHandler([
            new Response(200),
            new Response(403),
            new Response(404)
        ]);
    
        $handler = HandlerStack::create($mock);
        $client = new Client(new HttpClient(['handler' => $handler]));
    
        $resource = new InboundResource($client);
    
        $this->assertTrue($resource->removeShippingNotification('MERC03INBND01', 'Foobar'));
    
        $this->expectException(ClientException::class);
        $resource->removeShippingNotification('MERC03INBND01', 'Foobar');
    
        $this->assertFalse($resource->removeShippingNotification('MERC03INBND01', 'Foobar'));
    }
}
