<?php
namespace Jtl\Fulfillment\Api\Sdk\Test\Resources\Merchant;

use Izzle\Model\PropertyInfo;
use Jtl\Fulfillment\Api\Sdk\Models\Merchant\Fulfiller\Fulfiller;
use Jtl\Fulfillment\Api\Sdk\Resources\Merchant\FulfillerResource;
use Jtl\Fulfillment\Api\Sdk\Test\Resources\AbstractResourceTest;

/**
 * Class FulfillerTest
 * @package Jtl\Fulfillment\Api\Sdk\Test\Resources\Merchant
 */
class FulfillerTest extends AbstractResourceTest
{
    public function testCanQueryAll(): void
    {
        $this->canQueryAll(__DIR__ . '/MockData/fulfiller_all.json', FulfillerResource::class, Fulfiller::class);
    }
    
    public function testCanFindOne(): void
    {
        $this->canFindOne(
            __DIR__ . '/MockData/fulfiller_one.json',
            FulfillerResource::class,
            Fulfiller::class,
            'ABCD',
            new PropertyInfo('userId')
        );
    }
}
