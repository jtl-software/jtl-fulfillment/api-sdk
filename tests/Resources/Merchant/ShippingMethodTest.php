<?php
namespace Jtl\Fulfillment\Api\Sdk\Test\Resources\Merchant;

use Izzle\Model\PropertyInfo;
use Jtl\Fulfillment\Api\Sdk\Models\General\ShippingMethod;
use Jtl\Fulfillment\Api\Sdk\Resources\Merchant\ShippingMethodResource;
use Jtl\Fulfillment\Api\Sdk\Test\Resources\AbstractResourceTest;

/**
 * Class ShippingMethodTest
 * @package Jtl\Fulfillment\Api\Sdk\Test\Resources\Merchant
 */
class ShippingMethodTest extends AbstractResourceTest
{
    public function testCanQueryAll(): void
    {
        $this->canQueryAll(__DIR__ . '/MockData/shipping_method_all.json', ShippingMethodResource::class, ShippingMethod::class);
    }
    
    public function testCanFindOne(): void
    {
        $this->canFindOne(
            __DIR__ . '/MockData/shipping_method_one.json',
            ShippingMethodResource::class,
            ShippingMethod::class,
            'FULF0ASHPMETHD01',
            new PropertyInfo('shippingMethodId')
        );
    }
}
