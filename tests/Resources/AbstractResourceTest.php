<?php
namespace Jtl\Fulfillment\Api\Sdk\Test\Resources;

use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use InvalidArgumentException;
use Izzle\Model\Model;
use Izzle\Model\PropertyInfo;
use Jtl\Fulfillment\Api\Sdk\Client;
use Jtl\Fulfillment\Api\Sdk\Models\Pagination;
use Jtl\Fulfillment\Api\Sdk\Models\Query;
use Jtl\Fulfillment\Api\Sdk\Models\TimeFrame;
use Jtl\Fulfillment\Api\Sdk\Resources\ResourceInterface;
use PHPUnit\Framework\TestCase;
use GuzzleHttp\Client as HttpClient;
use ReflectionException;

/**
 * Class ResourceTest
 * @package Jtl\Fulfillment\Api\Sdk\Test\Resources
 */
abstract class AbstractResourceTest extends TestCase
{
    /**
     * @param string $jsonFile
     * @param string $resourceClass
     * @param string $resultClass
     * @param string $method
     * @param callable|null $assertions
     * @throws ReflectionException
     */
    public function canQueryAll(
        string $jsonFile,
        string $resourceClass,
        string $resultClass,
        string $method = 'all',
        callable $assertions = null
    ): void {
        $mock = new MockHandler([
            new Response(200, [], file_get_contents($jsonFile)),
            new Response(403),
            new Response(404)
        ]);
        
        $handler = HandlerStack::create($mock);
        $client = new Client(new HttpClient(['handler' => $handler]));
        
        /** @var ResourceInterface $resource */
        $resource = new $resourceClass($client);
        
        /** @var Pagination $result */
        $result = $resource->{$method}(new Query());

        if (is_callable($assertions)) {
            $assertions($result);
        }

        $items = $result->getItems();
        
        $this->assertCount(1, $items);
        $this->assertInstanceOf($resultClass, $items[0]);
    
        $this->expectException(ClientException::class);
        $resource->{$method}(new Query());
    
        $this->assertEmpty($resource->{$method}(new Query()));
    }
    
    /**
     * @param string $jsonFile
     * @param string $resourceClass
     * @param string $resultClass
     * @param string $method
     * @param callable|null $assertions
     */
    public function canQueryUpdates(
        string $jsonFile,
        string $resourceClass,
        string $resultClass,
        string $method = 'all',
        callable $assertions = null
    ): void {
        $client = $this->prepareHttpHandler(file_get_contents($jsonFile));
        
        /** @var ResourceInterface $resource */
        $resource = new $resourceClass($client);
        
        /** @var TimeFrame $result */
        $result = $resource->{$method}(new Query());

        if (is_callable($assertions)) {
            $assertions($result);
        }

        $items = $result->getData();

        $this->assertCount(1, $items);
        $this->assertInstanceOf($resultClass, $items[0]);
        
        $this->expectException(ClientException::class);
        $resource->{$method}(new Query());
        
        $this->assertEmpty($resource->{$method}(new Query()));
    }
    
    /**
     * @param string $jsonFile
     * @param string $resourceClass
     * @param string $resultClass
     * @param string $id
     * @param PropertyInfo $idProperty
     */
    public function canFindOne(
        string $jsonFile,
        string $resourceClass,
        string $resultClass,
        string $id,
        PropertyInfo $idProperty
    ): void {
        $client = $this->prepareHttpHandler(file_get_contents($jsonFile));
    
        /** @var ResourceInterface $resource */
        $resource = new $resourceClass($client);
    
        $model = $resource->find($id);
        $getter = $idProperty->getter();
    
        $this->assertInstanceOf($resultClass, $model);
        $this->assertEquals($id, $model->{$getter}());
    
        $this->expectException(ClientException::class);
        $resource->find($id);
    
        $this->assertEmpty($resource->find($id));
    }
    
    /**
     * @param string $jsonFile
     * @param string $resourceClass
     * @param string $resultClass
     * @param string $method
     * @param string $id
     * @param PropertyInfo|null $idProperty
     * @throws InvalidArgumentException
     */
    public function canFindOneBy(
        string $jsonFile,
        string $resourceClass,
        string $resultClass,
        string $method,
        string $id,
        PropertyInfo $idProperty = null
    ): void {
        $client = $this->prepareHttpHandler(file_get_contents($jsonFile));
        
        /** @var ResourceInterface $resource */
        $resource = new $resourceClass($client);
        
        if (!is_callable([$resource, $method])) {
            throw new InvalidArgumentException(sprintf('Method %s is not callable on %s', $method, $resourceClass));
        }
    
        $model = $resource->{$method}($id);
        
        if ($idProperty !== null) {
            $getter = $idProperty->getter();
            $this->assertEquals($id, $model->{$getter}());
        }
        
        $this->assertInstanceOf($resultClass, $model);
    
        $this->expectException(ClientException::class);
        $resource->{$method}($id);
    
        $this->assertEmpty($resource->{$method}($id));
    }
    
    /**
     * @param string $jsonFile
     * @param string $resourceClass
     * @param string $resultClass
     * @param string $id
     * @param PropertyInfo $idProperty
     * @param Query|null $query
     */
    public function canUpdate(
        string $jsonFile,
        string $resourceClass,
        string $resultClass,
        string $id,
        PropertyInfo $idProperty,
        Query $query = null
    ): void {
        $json = file_get_contents($jsonFile);
        $client = $this->prepareHttpHandler(null, 204);
    
        /** @var ResourceInterface $resource */
        $resource = new $resourceClass($client);
        
        /** @var Model $model */
        $model = new $resultClass(json_decode($json, true));
        $result = $resource->save($model, $query);
    
        $getter = $idProperty->getter();
    
        $this->assertTrue($result);
        $this->assertEquals($id, $model->{$getter}());
    
        $this->expectException(ClientException::class);
        $resource->save($model, $query);
    
        $this->assertEmpty($resource->save($model, $query));
    }
    
    /**
     * @param string $jsonFile
     * @param string $resourceClass
     * @param string $resultClass
     * @param string|null $id
     * @param PropertyInfo|null $idProperty
     * @param Query|null $query
     * @param string $method
     */
    public function canCreate(
        string $jsonFile,
        string $resourceClass,
        string $resultClass,
        string $id = null,
        PropertyInfo $idProperty = null,
        Query $query = null,
        string $method = 'save'
    ): void {
        $json = file_get_contents($jsonFile);
        $client = $this->prepareHttpHandler($json, 201);
    
        /** @var ResourceInterface $resource */
        $resource = new $resourceClass($client);
    
        /** @var Model $model */
        $model = new $resultClass(json_decode($json, true));
    
        if ($id !== null && $idProperty !== null) {
            $getter = $idProperty->getter();
            $setter = $idProperty->setter();
    
            $model->{$setter}('');
            
            // Modification fix
            if (is_callable([$model, 'setModificationInfo'])) {
                $model->setModificationInfo(null);
            }
        }
        
        $result = $resource->{$method}($model, $query);
        
        $this->assertTrue($result);
    
        if ($id !== null && $idProperty !== null) {
            $this->assertEquals($id, $model->{$getter}());
        }
    
        $this->expectException(ClientException::class);
        $resource->{$method}($model, $query);
    
        $this->assertEmpty($resource->{$method}($model, $query));
    }
    
    /**
     * @param string $jsonFile
     * @param string $resourceClass
     * @param string $resultClass
     * @param Query|null $query
     * @param string $method
     */
    public function canRemove(
        string $jsonFile,
        string $resourceClass,
        string $resultClass,
        Query $query = null,
        string $method = 'remove'
    ): void {
        $json = file_get_contents($jsonFile);
        $client = $this->prepareHttpHandler(null, 204);
        
        /** @var ResourceInterface $resource */
        $resource = new $resourceClass($client);
    
        /** @var Model $model */
        $model = new $resultClass(json_decode($json, true));
    
        $this->assertTrue($resource->{$method}($model, $query));
        
        $this->expectException(ClientException::class);
        $resource->{$method}($model, $query);
    
        $this->assertFalse($resource->{$method}($model, $query));
    }
    
    /**
     * @param string $json |null
     * @param int $success
     * @param int $forbidden
     * @param int $notFound
     * @return Client
     */
    protected function prepareHttpHandler(
        string $json = null,
        int $success = 200,
        int $forbidden = 403,
        int $notFound = 404
    ): Client {
        $response = $json !== null ?
            new Response($success, [], $json) :
            new Response($success);
        
        $mock = new MockHandler([
            $response,
            new Response($forbidden),
            new Response($notFound)
        ]);
    
        $handler = HandlerStack::create($mock);
        
        return new Client(new HttpClient(['handler' => $handler]));
    }
}
