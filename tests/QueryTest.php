<?php
namespace Jtl\Fulfillment\Api\Sdk\Test;

use InvalidArgumentException;
use Jtl\Fulfillment\Api\Sdk\Models\Query;
use Jtl\Fulfillment\Api\Sdk\Models\QueryParam;
use Jtl\Fulfillment\Api\Sdk\Query\Builder;
use Jtl\Fulfillment\Api\Sdk\Query\Grammars\FilterGrammar;
use Jtl\Fulfillment\Api\Sdk\Query\Grammars\ODataGrammar;
use PHPUnit\Framework\TestCase;

/**
 * Class QueryTest
 * @package Jtl\Fulfillment\Api\Sdk\Test
 */
class QueryTest extends TestCase
{
    /**
     * @var Builder
     */
    private $builder;
    
    public function setUp(): void
    {
        parent::setUp();
        
        $this->builder = new Builder();
    }
    
    public function testBuilderCanHoldWheres(): void
    {
        $this->builder->where('foo', 'bar');
        
        $this->assertCount(1, $this->builder->wheres);
    }
    
    public function testBuilderCanBeCompiled(): void
    {
        $this->builder->where('foo', 'bar');
        
        $grammar = new FilterGrammar();
        $this->assertEquals('filter[]=foo=bar', $grammar->compileWheres($this->builder));
        
        $wheres = $grammar->compileWheresToArray($this->builder);
        $this->assertCount(1, $wheres);
        $this->assertEquals('foo=bar', $wheres[0]);
        
        $grammar = new ODataGrammar();
        $this->assertEquals('$filter=foo eq \'bar\'', $grammar->compileWheres($this->builder));
    
        $wheres = $grammar->compileWheresToArray($this->builder);
        $this->assertCount(1, $wheres);
        $this->assertEquals('foo eq \'bar\'', $wheres[0]);
    }
    
    public function testBuilderCanHoldComplexWheres(): void
    {
        $this->builder->where('foo', 'bar')->where('count', Builder::OPERATOR_GREATER_THAN, 3)
            ->orWhere('name', Builder::OPERATOR_NOT_EQUAL, 'hans');
    
        $fg = new FilterGrammar();
        $this->assertEquals('filter[]=foo=bar,count>3&filter[]=name!=hans', $fg->compileWheres($this->builder));
    
        $dg = new ODataGrammar();
        $this->assertEquals('$filter=foo eq \'bar\' and count gt 3 or name ne \'hans\'', $dg->compileWheres($this->builder));
    
        $this->builder->clean();
    
        // Normal Wheres
        $this->builder->where([
            ['foo', 'bar'],
            ['x', 'y'],
        ]);
    
        $this->assertEquals('filter[]=foo=bar,x=y', $fg->compileWheres($this->builder));
        $this->assertEquals('$filter=(foo eq \'bar\' and x eq \'y\')', $dg->compileWheres($this->builder));
    
        $this->builder->clean();
    
        // Normal OR Wheres
        $this->builder->orWhere('foo', 'bar')->orWhere('x', 'y');
        $this->assertEquals('$filter=foo eq \'bar\' or x eq \'y\'', $dg->compileWheres($this->builder));
    
        $this->builder->clean();
    
        // Where IN
        $this->builder->whereIn('name', ['a', 'b', 'c']);
        $this->assertEquals('$filter=name in (\'a\',\'b\',\'c\')', $dg->compileWheres($this->builder));
    
        $this->builder->clean();
    
        // Where SUB (Object)
        $this->builder->whereSub('items', 'jfsku', Builder::OPERATOR_EQUAL, 'foobar');
        $this->assertEquals('$filter=items/any(x:x/jfsku eq \'foobar\')', $dg->compileWheres($this->builder));
    
        $this->builder->clean();
    
        // Where SUB (Array)
        $this->builder->whereSub('items', null, Builder::OPERATOR_EQUAL, 'foobar');
        $this->assertEquals('$filter=items/any(x:x eq \'foobar\')', $dg->compileWheres($this->builder));
    
        $this->builder->clean();
        
        // Where Contains
        $this->builder->whereContains('sku', 'foobar')->orWhereContains('sku', '32');
        $this->assertEquals('$filter=contains(sku, \'foobar\') or contains(sku, \'32\')', $dg->compileWheres($this->builder));
    
        $this->builder->clean();
    
        // Where Starts With
        $this->builder->whereStartsWith('sku', 'foobar')->orWhereStartsWith('sku', '32');
        $this->assertEquals('$filter=startswith(sku, \'foobar\') or startswith(sku, \'32\')', $dg->compileWheres($this->builder));
    
        $this->builder->clean();
    
        // Where Ends With
        $this->builder->whereEndsWith('sku', 'foobar')->orWhereEndsWith('sku', '32');
        $this->assertEquals('$filter=endswith(sku, \'foobar\') or endswith(sku, \'32\')', $dg->compileWheres($this->builder));
        
        $this->builder->clean();
        
        // Complex Query
        $this->builder->with(['stock','specifications($expand=billOfMaterialsComponents($select=MerchantSku,name))']);
        $this->assertEquals('$expand=stock,specifications($expand=billOfMaterialsComponents($select=MerchantSku,name))', $dg->compileExpands($this->builder));
    
        $query = new Query();
        $query->with(['stock','specifications($expand=billOfMaterialsComponents($select=MerchantSku,name))']);
        $data = $query->toArray();
        $this->assertArrayHasKey('$expand', $data);
        $this->assertEquals('stock,specifications($expand=billOfMaterialsComponents($select=MerchantSku,name))', $data['$expand']);
    }
    
    public function testCanBeSerializedToArray(): void
    {
        Query::$oDataSerialization = false;
        $query = new Query([
            'limit' => 5,
            'offset' => 0,
            'params' => [
                [
                    'key' => 'foo',
                    'value' => 'bar'
                ]
            ]
        ]);
    
        $query->where('foo', 'bar');
        
        $data = $query->toArray();
        
        $this->assertIsArray($data);
        $this->assertArrayHasKey('limit', $data);
        $this->assertArrayHasKey('offset', $data);
        $this->assertArrayHasKey('filter', $data);
        $this->assertArrayHasKey('foo', $data);
    
        $this->assertEquals('bar', $data['foo']);
    }
    
    public function testCanBeSerializedToODataArray(): void
    {
        Query::$oDataSerialization = true;
        $query = new Query([
            'limit' => 5,
            'offset' => 0,
        ]);
    
        $query->where('foo', 'bar');
        $data = $query->toArray();
    
        $this->assertIsArray($data);
        $this->assertArrayHasKey('$top', $data);
        $this->assertArrayHasKey('$skip', $data);
        $this->assertArrayHasKey('$filter', $data);
    }
    
    public function testCanAddQueryParams(): void
    {
        $query = new Query();
        $query->setParams([
            new QueryParam([
                'key' => 'foo',
                'value' => 'bar',
            ]),
            new QueryParam([
                'key' => 'x',
                'value' => 'y',
            ]),
        ]);
    
        $params = $query->getParams();
        
        $this->assertCount(2, $params);
        $this->assertArrayHasKey('foo', $params);
        $this->assertArrayHasKey('x', $params);
        $this->assertEquals('bar', $params['foo']->getValue());
        $this->assertEquals('y', $params['x']->getValue());
        
        $this->expectException(InvalidArgumentException::class);
        $query->setParams([1, 2]);
    }
    
    public function testCanAddQueryParam(): void
    {
        $query = new Query();
        $query->addParam(
            new QueryParam([
                'key' => 'foo',
                'value' => 'bar',
            ])
        );
        
        $this->assertCount(1, $query->getParams());
        
        $param = $query->getParam('foo');
        $this->assertNotNull($param);
        $this->assertEquals('bar', $param->getValue());
    
        $param = $query->getParam('x');
        $this->assertNull($param);
    }
    
    public function testCanRemoveQueryParam(): void
    {
        $fooParam = new QueryParam([
            'key' => 'foo',
            'value' => 'bar',
        ]);
        
        $query = new Query();
        $query->setParams([
            $fooParam->getKey() => $fooParam,
            'x' => new QueryParam([
                'key' => 'x',
                'value' => 'y',
            ]),
        ]);
    
        $this->assertCount(1, $query->removeParam($fooParam)->getParams());
    }
    
    public function testCanRemoveQueryParamByKey(): void
    {
        $fooParam = new QueryParam([
            'key' => 'foo',
            'value' => 'bar',
        ]);
    
        $query = new Query();
        $query->setParams([
            $fooParam->getKey() => $fooParam,
            'x' => new QueryParam([
                'key' => 'x',
                'value' => 'y',
            ]),
        ]);
    
        $this->assertCount(1, $query->removeParamByKey($fooParam->getKey())->getParams());
    }
    
    public function testCanBuildQueryString(): void
    {
        $query = new Query();
        $query->setParams([
            'foo' => new QueryParam([
                'key' => 'foo',
                'value' => 'bar',
            ]),
            'x' => new QueryParam([
                'key' => 'x',
                'value' => 'y',
            ]),
        ]);
        
        $this->assertEquals('?foo=bar&x=y', $query->buildQueryString());
    }
}
