<?php
namespace Jtl\Fulfillment\Api\Sdk\Test\Exceptions;

use Exception;
use Jtl\Fulfillment\Api\Sdk\Exceptions\MalformedDataException;
use PHPUnit\Framework\TestCase;

/**
 * Class MalformedDataTest
 * @package Jtl\Fulfillment\Api\Sdk\Test\Exceptions
 */
class MalformedDataTest extends TestCase
{
    public function testMalformedDataExceptionExtendsException(): void
    {
        $this->assertInstanceOf(Exception::class, new MalformedDataException());
    }
}
