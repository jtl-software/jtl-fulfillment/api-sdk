<?php
namespace Jtl\Fulfillment\Api\Sdk\Test\Exceptions;

use Exception;
use Jtl\Fulfillment\Api\Sdk\Exceptions\NotImplementedException;
use PHPUnit\Framework\TestCase;

/**
 * Class NotImplementedTest
 * @package Jtl\Fulfillment\Api\Sdk\Test\Exceptions
 */
class NotImplementedTest extends TestCase
{
    public function testNotImplementedExceptionExtendsException(): void
    {
        $this->assertInstanceOf(Exception::class, new NotImplementedException());
    }
}
