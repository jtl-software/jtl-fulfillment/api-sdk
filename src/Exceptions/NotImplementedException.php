<?php
namespace Jtl\Fulfillment\Api\Sdk\Exceptions;

use Exception;

/**
 * Class NotImplementedException
 * @package Jtl\Fulfillment\Api\Sdk\Exceptions
 */
class NotImplementedException extends Exception
{
}
