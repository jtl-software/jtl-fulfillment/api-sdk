<?php
namespace Jtl\Fulfillment\Api\Sdk\Exceptions;

use Exception;

/**
 * Class JsonException
 * @package Jtl\Fulfillment\Api\Sdk\Exceptions
 */
class JsonException extends Exception
{
}
