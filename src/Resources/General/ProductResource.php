<?php
namespace Jtl\Fulfillment\Api\Sdk\Resources\General;

use finfo;
use Jtl\Fulfillment\Api\Sdk\Exceptions\HttpException;
use Throwable;
use InvalidArgumentException;
use Jtl\Fulfillment\Api\Sdk\Exceptions\JsonException;
use Jtl\Fulfillment\Api\Sdk\Models\General\Product\ProductPicture;
use Jtl\Fulfillment\Api\Sdk\Resources\Resource;

/**
 * Class ProductResource
 * @package Jtl\Fulfillment\Api\Sdk\Resources\General
 */
class ProductResource extends Resource
{
    /**
     * @param string $url
     * @return ProductPicture|null
     * @throws Throwable
     * @throws JsonException
     */
    public function findPictureFromUrl(string $url): ?ProductPicture
    {
        // Try Cache
        $cacheKey = $this->buildCacheKey($url);
        $cachedItem = $this->resourceCache->get($cacheKey);
        if ($cachedItem !== null) {
            return $cachedItem;
        }
    
        try {
            $response = $this->getClient()->getHttp()->request('GET', $url);
    
            $picture = new ProductPicture($this->extractData($response));
    
            // Set Cache
            $this->resourceCache->set($picture, $cacheKey);
    
            return $picture;
        } catch (Throwable $e) {
            HttpException::handleGuzzeException($e);
        }
    
        return null;
    }
    
    /**
     * @param string $url
     * @return string|null
     * @throws Throwable
     */
    public function loadDataFromUrl(string $url): ?string
    {
        try {
            $response = $this->getClient()->getHttp()->request('GET', $url, ['base_uri' => '']);
        
            return base64_encode($response->getBody()->getContents());
        } catch (Throwable $e) {
            HttpException::handleGuzzeException($e);
        }
    
        return null;
    }
    
    /**
     * @param string $id
     * @return ProductPicture|null
     * @throws Throwable
     * @throws JsonException
     */
    public function findPictureByProductId(string $id): ?ProductPicture
    {
        // Try Cache
        $cacheKey = $this->buildCacheKey($id . '1', 'picture');
        $cachedItem = $this->resourceCache->get($cacheKey);
        if ($cachedItem !== null) {
            return $cachedItem;
        }
    
        try {
            $response = $this->getClient()->getHttp()->request('GET', sprintf('pictures/%s', $id));
    
            $picture = new ProductPicture($this->extractData($response));
    
            // Set Cache
            $this->resourceCache->set($picture, $cacheKey);
    
            return $picture;
        } catch (Throwable $e) {
            HttpException::handleGuzzeException($e);
        }
    
        return null;
    }
    
    /**
     * @param ProductPicture $picture
     * @return string
     * @throws InvalidArgumentException
     */
    public function getHtmlSource(ProductPicture $picture): string
    {
        if ($picture->getData() === null) {
            throw new InvalidArgumentException('Picture data is null');
        }
        
        $finfo = new finfo(FILEINFO_MIME_TYPE);
        
        return sprintf(
            'data:%s;base64,%s',
            $finfo->buffer(base64_decode($picture->getData())),
            $picture->getData()
        );
    }
}
