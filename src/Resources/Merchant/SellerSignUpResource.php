<?php
namespace Jtl\Fulfillment\Api\Sdk\Resources\Merchant;

use Exception;
use Jtl\Fulfillment\Api\Sdk\Exceptions\JsonException;
use Jtl\Fulfillment\Api\Sdk\Models\Merchant\Amazon\Seller;
use Jtl\Fulfillment\Api\Sdk\Models\Merchant\Amazon\SignUp;
use Jtl\Fulfillment\Api\Sdk\Resources\Resource;

/**
 * Class SellerSignUpResource
 * @package Jtl\Fulfillment\Api\Sdk\Resources\Merchant
 */
class SellerSignUpResource extends Resource
{
    /**
     * @param int $accountId
     * @param string $seller
     * @param string $marketplaceId
     * @param string|null $redirectUrl
     * @return SignUp
     * @throws JsonException
     */
    public function initSignUp(int $accountId, string $seller, string $marketplaceId, ?string $redirectUrl = null): SignUp
    {
        $params = [
            'jtlAccountId' => $accountId,
            'name' => $seller,
            'marketplaceId' => $marketplaceId
        ];

        if ($redirectUrl !== null) {
            $params['redirectUrl'] = $redirectUrl;
        }

        $response = $this->getClient()->getHttp()->request(
            'GET',
            'ffn/amazon/init-signup-session',
            [
                'query' => $params
            ]
        );

        $data = $this->extractData($response);

        return new SignUp($data['data']);
    }

    /**
     * @param int $accountId
     * @return Seller[]
     * @throws Exception
     */
    public function getSellers(int $accountId): array
    {
        $response = $this->getClient()->getHttp()->request(
            'GET',
            sprintf('ffn/amazon/seller/%s', $accountId)
        );

        $result = [];

        $data = $this->extractData($response);

        if (!isset($data['data']['sellerList']) || !is_array($data['data']['sellerList'])) {
            return $result;
        }

        foreach ($data['data']['sellerList'] as $seller) {
            $result[] = new Seller($seller);
        }

        return $result;
    }
}
