<?php
namespace Jtl\Fulfillment\Api\Sdk\Resources\Merchant;

use Jtl\Fulfillment\Api\Sdk\Exceptions\HttpException;
use Throwable;
use Izzle\Model\Model;
use Jtl\Fulfillment\Api\Sdk\Exceptions\JsonException;
use Jtl\Fulfillment\Api\Sdk\Models\Merchant\Product\Product;
use Jtl\Fulfillment\Api\Sdk\Models\General\Product\ProductPicture;
use Jtl\Fulfillment\Api\Sdk\Models\Merchant\Product\ProductAuthorization;
use Jtl\Fulfillment\Api\Sdk\Models\Pagination;
use Jtl\Fulfillment\Api\Sdk\Models\Query;
use Jtl\Fulfillment\Api\Sdk\Resources\Resource;

/**
 * Class ProductResource
 * @package Jtl\Fulfillment\Api\Sdk\Resources\Merchant
 */
class ProductResource extends Resource
{
    /**
     * @param Query $query
     * @return Pagination
     * @throws Throwable
     * @throws JsonException
     */
    public function all(Query $query): Pagination
    {
        return $this->findAll('merchant/products', Product::class, $query, $this->buildCacheKey((string) $query));
    }
    
    /**
     * @param string $id
     * @param Query|null $query
     * @return Product|Model|null
     * @throws Throwable
     * @throws JsonException
     */
    public function find(string $id, Query $query = null): ?Model
    {
        return $this->findBy(sprintf('merchant/products/%s', $id), Product::class, $this->buildCacheKey($id), $query);
    }
    
    /**
     * @param Product|Model $model
     * @param Query|null $query
     * @return bool
     * @throws Throwable
     * @throws JsonException
     */
    public function save(Model $model, Query $query = null): bool
    {
        $this->validModel($model, Product::class);
        
        /** @var Product $model */
        return empty($model->getJfsku()) ?
            $this->create($model, 'merchant/products', Product::class, $model->property('jfsku')) :
            $this->update(
                $model,
                sprintf('merchant/products/%s', $model->getJfsku()),
                $this->buildCacheKey($model->getJfsku())
            );
    }
    
    /**
     * @param Model $model
     * @param Query|null $query
     * @return bool
     * @throws Throwable
     */
    public function remove(Model $model, Query $query = null): bool
    {
        $this->validModel($model, Product::class);
        
        /** @var Product $model */
        $result = $this->delete(
            sprintf('merchant/products/%s', $model->getJfsku()),
            $this->buildCacheKey($model->getJfsku())
        );
        
        if ($result) {
            // Remove picture Cache
            /*
            $this->getResourceCache()->deleteMultiple(
                $this->getResourceCache()->filterKeys('/^picture-' . $model->getJfsku() . '_.+/i')
            );
            */
        }
        
        return $result;
    }
    
    /**
     * @param string $jfsku
     * @return ProductAuthorization[]
     * @throws JsonException
     * @throws Throwable
     */
    public function findAuthorizations(string $jfsku): array
    {
        $results = [];
        
        try {
            $response = $this->getClient()->getHttp()->request(
                'GET',
                sprintf('merchant/products/%s/authorizations', $jfsku)
            );
    
            $data = $this->extractData($response);
            foreach ($data as $d) {
                $results[] = new ProductAuthorization($d);
            }
        } catch (Throwable $e) {
            HttpException::handleGuzzeException($e);
        }
    
        return $results;
    }
    
    /**
     * @param string $jfsku
     * @param string $fulfillerId
     * @return ProductAuthorization|null
     * @throws JsonException
     * @throws Throwable
     */
    public function grantAccess(string $jfsku, string $fulfillerId): ?ProductAuthorization
    {
        try {
            $response = $this->getClient()->getHttp()->request(
                'POST',
                sprintf('merchant/products/%s/authorizations', $jfsku),
                [
                    'body' => json_encode(['fulfillerId' => $fulfillerId])
                ]
            );
        
            return new ProductAuthorization($this->extractData($response));
        } catch (Throwable $e) {
            HttpException::handleGuzzeException($e);
        }
        
        return null;
    }
    
    /**
     * @param string $jfsku
     * @param string $fulfillerId
     * @return bool
     * @throws Throwable
     */
    public function removeAccess(string $jfsku, string $fulfillerId): bool
    {
        try {
            $response = $this->getClient()->getHttp()->request(
                'DELETE',
                sprintf('merchant/products/%s/authorizations/%s', $jfsku, $fulfillerId)
            );
        
            return $response->getStatusCode() === 200;
        } catch (Throwable $e) {
            HttpException::handleGuzzeException($e);
        }
    
        return false;
    }
    
    /**
     * @param string $fulfillerId
     * @return bool
     * @throws Throwable
     */
    public function grantOverallAccessTo(string $fulfillerId): bool
    {
        $response = $this->getClient()->getHttp()->request(
            'POST',
            'merchant/products/authorizations',
            [
                'body' => json_encode(['fulfillerId' => $fulfillerId])
            ]
        );
    
        return $response->getStatusCode() === 201;
    }
    
    /**
     * @param string $jfsku
     * @param string $data - base64 encoded picture data
     * @param int $number
     * @return ProductPicture|null
     * @throws JsonException
     * @throws Throwable
     */
    public function addPicture(string $jfsku, string $data, int $number = 0): ?ProductPicture
    {
        try {
            $response = $this->getClient()->getHttp()->request(
                'POST',
                sprintf('merchant/products/%s/pictures', $jfsku),
                [
                    'body' => json_encode([
                        'data' => $data,
                        'number' => $number
                    ])
                ]
            );
            
            $this->getResourceCache()->delete($this->buildCacheKey($jfsku));
    
            // Delete Page Cache
            $this->deletePageCache();
        
            $picture = new ProductPicture($this->extractData($response));
            
            // Set Cache
            $this->getResourceCache()->set($picture, $this->buildCacheKey('picture', sprintf('%s_%s', $jfsku, $number)));
            
            return $picture;
        } catch (Throwable $e) {
            HttpException::handleGuzzeException($e);
        }
    
        return null;
    }
    
    /**
     * @param string $jfsku
     * @param int $number
     * @return ProductPicture|null
     * @throws JsonException
     * @throws Throwable
     */
    public function findPicture(string $jfsku, int $number = 0): ?ProductPicture
    {
        $cacheKey = $this->buildCacheKey('picture', sprintf('%s_%s', $jfsku, $number));
        
        // Try Cache
        $cachedItem = $this->getResourceCache()->get($cacheKey);
        if ($cachedItem !== null) {
            return $cachedItem;
        }
        
        try {
            $response = $this->getClient()->getHttp()->request(
                'GET',
                sprintf('merchant/products/%s/pictures/%s', $jfsku, $number)
            );
            
            $picture = new ProductPicture($this->extractData($response));
    
            // Set Cache
            $this->getResourceCache()->set($picture, $cacheKey);
    
            return $picture;
        } catch (Throwable $e) {
            HttpException::handleGuzzeException($e);
        }
        
        return null;
    }
    
    /**
     * @param string $jfsku
     * @param int $number
     * @return bool
     * @throws Throwable
     */
    public function removePicture(string $jfsku, int $number): bool
    {
        try {
            $response = $this->getClient()->getHttp()->request(
                'DELETE',
                sprintf('merchant/products/%s/pictures/%s', $jfsku, $number)
            );
    
            $result = $response->getStatusCode() === 200;
            if ($result) {
                $this->getResourceCache()->delete($this->buildCacheKey($jfsku));
        
                // Delete Page Cache
                $this->deletePageCache();
                
                $this->getResourceCache()->delete($this->buildCacheKey('picture', sprintf('%s_%s', $jfsku, $number)));
            }
    
            return $result;
        } catch (Throwable $e) {
            HttpException::handleGuzzeException($e);
        }
        
        return false;
    }
}
