<?php
namespace Jtl\Fulfillment\Api\Sdk\Resources\Merchant;

use Jtl\Fulfillment\Api\Sdk\Models\Merchant\Stock\StockComplete;
use Jtl\Fulfillment\Api\Sdk\Models\TimeChunk;
use Throwable;
use Izzle\Model\Model;
use Jtl\Fulfillment\Api\Sdk\Client;
use Jtl\Fulfillment\Api\Sdk\Exceptions\JsonException;
use Jtl\Fulfillment\Api\Sdk\Models\General\Product\ProductStockLevel;
use Jtl\Fulfillment\Api\Sdk\Models\General\Product\ProductStockLevelWarehouse;
use Jtl\Fulfillment\Api\Sdk\Models\Merchant\Stock\StockChange;
use Jtl\Fulfillment\Api\Sdk\Models\Pagination;
use Jtl\Fulfillment\Api\Sdk\Models\Query;
use Jtl\Fulfillment\Api\Sdk\Models\TimeFrame;
use Jtl\Fulfillment\Api\Sdk\Resources\Resource;
use Jtl\Fulfillment\Api\Sdk\Cache\NullCache;

/**
 * Class StockResource
 * @package Jtl\Fulfillment\Api\Sdk\Resources\Merchant
 */
class StockResource extends Resource
{
    /**
     * StockResource constructor.
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        parent::__construct($client);
        
        // Override global behavior and must be set, explizit on the resource controller
        $this->resourceCache->setCache(new NullCache());
    }
    
    /**
     * @param Query $query
     * @return Pagination
     * @throws Throwable
     * @throws JsonException
     */
    public function all(Query $query): Pagination
    {
        return $this->findAll('merchant/stocks', ProductStockLevel::class, $query, $this->buildCacheKey((string) $query));
    }
    
    /**
     * @param string $warehouseId
     * @param Query $query
     * @return Pagination
     * @throws Throwable
     * @throws JsonException
     */
    public function allByWarehouseId(string $warehouseId, Query $query): Pagination
    {
        return $this->findAll(
            sprintf('merchant/stocks/warehouse/%s', $warehouseId),
            ProductStockLevel::class,
            $query,
            $this->buildCacheKey((string) $query, $warehouseId)
        );
    }
    
    /**
     * @param string $jfsku
     * @return ProductStockLevel|Model|null
     * @throws Throwable
     * @throws JsonException
     */
    public function findBySku(string $jfsku): ?ProductStockLevel
    {
        return $this->findBy(
            sprintf('merchant/stocks/%s', $jfsku),
            ProductStockLevel::class,
            $this->buildCacheKey($jfsku)
        );
    }
    
    /**
     * @param string $jfsku
     * @param string $warehouseId
     * @return ProductStockLevelWarehouse|Model|null
     * @throws Throwable
     * @throws JsonException
     */
    public function findBySkuAndWarehouse(string $jfsku, string $warehouseId): ?ProductStockLevelWarehouse
    {
        return $this->findBy(
            sprintf('merchant/stocks/%s/%s', $jfsku, $warehouseId),
            ProductStockLevelWarehouse::class,
            $this->buildCacheKey($jfsku . $warehouseId)
        );
    }
    
    /**
     * @param Query $query
     * @return TimeFrame
     * @throws Throwable
     * @throws JsonException
     */
    public function allUpdates(Query $query): TimeFrame
    {
        return $this->findUpdates(
            'merchant/stocks/updates',
            StockChange::class,
            $query,
            $this->buildCacheKey('merchant-updates-' . $query)
        );
    }
    
    /**
     * @param Query $query
     * @return Pagination
     * @throws Throwable
     * @throws JsonException
     */
    public function allChanges(Query $query): Pagination
    {
        return $this->findAll(
            'merchant/stocks/changes',
            StockChange::class,
            $query,
            $this->buildCacheKey('merchant-changes-' . $query)
        );
    }

    /**
     * @param Query $query
     * @return TimeChunk
     * @throws JsonException
     */
    public function allCompletes(Query $query): TimeChunk
    {
        return $this->findChunkUpdates(
            'merchant/stocks/complete',
            StockComplete::class,
            $query,
            $this->buildCacheKey('merchant-completes-' . $query)
        );
    }
}
