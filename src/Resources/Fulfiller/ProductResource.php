<?php
namespace Jtl\Fulfillment\Api\Sdk\Resources\Fulfiller;

use Jtl\Fulfillment\Api\Sdk\Exceptions\HttpException;
use Throwable;
use Izzle\Model\Model;
use Jtl\Fulfillment\Api\Sdk\Exceptions\JsonException;
use Jtl\Fulfillment\Api\Sdk\Models\Fulfiller\Product\Product;
use Jtl\Fulfillment\Api\Sdk\Models\General\Product\ProductPicture;
use Jtl\Fulfillment\Api\Sdk\Models\Pagination;
use Jtl\Fulfillment\Api\Sdk\Models\Query;
use Jtl\Fulfillment\Api\Sdk\Resources\Resource;

/**
 * Class ProductResource
 * @package Jtl\Fulfillment\Api\Sdk\Resources\Fulfiller
 */
class ProductResource extends Resource
{
    /**
     * @param Query $query
     * @return Pagination
     * @throws Throwable
     * @throws JsonException
     */
    public function all(Query $query): Pagination
    {
        return $this->findAll('fulfiller/products', Product::class, $query, $this->buildCacheKey((string) $query));
    }

    /**
     * @param string $id
     * @param Query|null $query
     * @return Product|Model|null
     * @throws Throwable
     * @throws JsonException
     */
    public function find(string $id, Query $query = null): ?Model
    {
        return $this->findBySku($id, $query);
    }

    /**
     * @param string $jfsku
     * @param Query|null $query
     * @return Model|Product|null
     * @throws Throwable
     * @throws JsonException
     */
    public function findBySku(string $jfsku, Query $query = null)
    {
        return $this->findBy(sprintf('fulfiller/products/%s', $jfsku), Product::class, $this->buildCacheKey($jfsku), $query);
    }

    /**
     * @param string $jfsku
     * @param int $number
     * @return ProductPicture|null
     * @throws Throwable
     * @throws JsonException
     */
    public function findPicture(string $jfsku, int $number): ?ProductPicture
    {
        try {
            $response = $this->getClient()->getHttp()->request(
                'GET',
                sprintf('fulfiller/products/%s/pictures/%s', $jfsku, $number)
            );

            return new ProductPicture($this->extractData($response));
        } catch (Throwable $e) {
            HttpException::handleGuzzeException($e);
        }

        return null;
    }
}
