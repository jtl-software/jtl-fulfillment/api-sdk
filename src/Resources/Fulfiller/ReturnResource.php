<?php
namespace Jtl\Fulfillment\Api\Sdk\Resources\Fulfiller;

use Izzle\Model\Model;
use Jtl\Fulfillment\Api\Sdk\Exceptions\HttpException;
use Jtl\Fulfillment\Api\Sdk\Exceptions\JsonException;
use Jtl\Fulfillment\Api\Sdk\Models\Fulfiller\Returns\FulfillerReturn;
use Jtl\Fulfillment\Api\Sdk\Models\Fulfiller\Returns\ReturnIncomingItem;
use Jtl\Fulfillment\Api\Sdk\Models\Fulfiller\Returns\ReturnItem as FulfillerReturnItem;
use Jtl\Fulfillment\Api\Sdk\Models\Fulfiller\Stock\StockChange;
use Jtl\Fulfillment\Api\Sdk\Models\Fulfiller\Returns\ReturnChangeShell;
use Jtl\Fulfillment\Api\Sdk\Models\Pagination;
use Jtl\Fulfillment\Api\Sdk\Models\Query;
use Jtl\Fulfillment\Api\Sdk\Models\TimeFrame;
use Jtl\Fulfillment\Api\Sdk\Resources\Resource;
use Throwable;

/**
 * Class ReturnResource
 * @package Jtl\Fulfillment\Api\Sdk\Resources\Fulfiller
 */
class ReturnResource extends Resource
{
    /**
     * @param Query $query
     * @return Pagination
     * @throws Throwable
     * @throws JsonException
     */
    public function all(Query $query): Pagination
    {
        return $this->findAll('fulfiller/returns', FulfillerReturn::class, $query, $this->buildCacheKey((string) $query));
    }

    /**
     * @param string $id
     * @param Query|null $query
     * @return FulfillerReturn|Model|null
     * @throws Throwable
     * @throws JsonException
     */
    public function find(string $id, Query $query = null): ?Model
    {
        return $this->findBy(sprintf('fulfiller/returns/%s', $id), FulfillerReturn::class, $this->buildCacheKey($id), $query);
    }

    /**
     * @param Query $query
     * @return TimeFrame
     * @throws Throwable
     * @throws JsonException
     */
    public function findUpdatesByTimeFrame(Query $query): TimeFrame
    {
        return $this->findUpdates(
            'fulfiller/returns/updates',
            FulfillerReturn::class,
            $query,
            $this->buildCacheKey('findUpdatesByTimeFrame' . $query)
        );
    }

    /**
     * @param Query $query
     * @return TimeFrame
     * @throws Throwable
     * @throws JsonException
     */
    public function findChangesByTimeFrame(Query $query): TimeFrame
    {
        return $this->findUpdates(
            'fulfiller/returns/changes',
            ReturnChangeShell::class,
            $query,
            $this->buildCacheKey('findChangesByTimeFrame' . $query)
        );
    }

    /**
     * @param string $id
     * @param Query $query
     * @return TimeFrame
     * @throws Throwable
     * @throws JsonException
     */
    public function findChangesForReturnByTimeFrame(string $id, Query $query): TimeFrame
    {
        return $this->findUpdates(
            sprintf('fulfiller/returns/{%s}/changes', $id),
            ReturnChangeShell::class,
            $query,
            $this->buildCacheKey($id . $query)
        );
    }

    /**
     * @param FulfillerReturn|Model $model
     * @param Query|null $query
     * @return bool
     * @throws Throwable
     * @throws JsonException
     */
    public function save(Model $model, Query $query = null): bool
    {
        $this->validModel($model, FulfillerReturn::class);

        /** @var FulfillerReturn $model */
        return empty($model->getReturnId()) ?
            $this->create($model, 'fulfiller/returns', FulfillerReturn::class, $model->property('returnId')) :
            $this->update(
                $model,
                sprintf('fulfiller/returns/%s', $model->getReturnId()),
                $this->buildCacheKey($model->getReturnId())
            );
    }

    /**
     * @param Model $model
     * @param Query|null $query
     * @return bool
     * @throws Throwable
     */
    public function remove(Model $model, Query $query = null): bool
    {
        $this->validModel($model, FulfillerReturn::class);

        /** @var FulfillerReturn $model */
        return $this->delete(
            sprintf('fulfiller/returns/%s', $model->getReturnId()),
            $this->buildCacheKey($model->getReturnId())
        );
    }

    /**
     * @param string $returnId
     * @param FulfillerReturnItem $item
     * @return FulfillerReturnItem|null
     * @throws Throwable
     */
    public function addItem(string $returnId, FulfillerReturnItem $item): ?FulfillerReturnItem
    {
        try {
            $response = $this->getClient()->getHttp()->request(
                'POST',
                sprintf('fulfiller/returns/%s/items', $returnId),
                [
                    'body' => json_encode([
                        'data' => self::removeEmpty($item, $item->toArray())
                    ])
                ]
            );

            $this->getResourceCache()->delete($this->buildCacheKey($returnId));

            // Delete Page Cache
            $this->deletePageCache();

            return new FulfillerReturnItem($this->extractData($response));
        } catch (Throwable $e) {
            HttpException::handleGuzzeException($e);
        }

        return null;
    }

    /**
     * @param string $returnId
     * @param string $itemId
     * @param float $quantity
     * @param string|null $merchantReturnItemId
     * @return FulfillerReturnItem|null
     * @throws Throwable
     */
    public function splitItem(string $returnId, string $itemId, float $quantity, ?string $merchantReturnItemId = null): ?FulfillerReturnItem
    {
        try {
            $body = [
                'quantity' => $quantity
            ];

            if ($merchantReturnItemId !== null) {
                $body['returnItemId'] = $merchantReturnItemId;
            }

            $response = $this->getClient()->getHttp()->request(
                'POST',
                sprintf('fulfiller/returns/%s/items/%s', $returnId, $itemId),
                [
                    'body' => json_encode($body)
                ]
            );

            $this->getResourceCache()->delete($this->buildCacheKey($returnId));

            // Delete Page Cache
            $this->deletePageCache();

            return new FulfillerReturnItem($this->extractData($response));
        } catch (Throwable $e) {
            HttpException::handleGuzzeException($e);
        }

        return null;
    }

    /**
     * @param string $returnId
     * @param string $itemId
     * @return FulfillerReturnItem|Model|null
     * @throws Throwable
     */
    public function findItem(string $returnId, string $itemId): ?Model
    {
        return $this->findBy(
            sprintf('fulfiller/returns/%s/items/%s', $returnId, $itemId),
            FulfillerReturnItem::class,
            $this->buildCacheKey($returnId . '_' . $itemId)
        );
    }

    /**
     * @param string $returnId
     * @param FulfillerReturnItem $item
     * @return bool
     * @throws Throwable
     */
    public function updateItem(string $returnId, FulfillerReturnItem $item): bool
    {
        return $this->update(
            $item,
            sprintf('fulfiller/returns/%s/items/%s', $returnId, $item->getReturnItemId()),
            $this->buildCacheKey($returnId . '_' . $item->getReturnItemId())
        );
    }

    /**
     * @param string $returnId
     * @param string $itemId
     * @return bool
     * @throws Throwable
     */
    public function removeItem(string $returnId, string $itemId): bool
    {
        return $this->delete(
            sprintf('fulfiller/returns/%s/items/%s', $returnId, $itemId),
            $this->buildCacheKey($returnId . '_' . $itemId)
        );
    }

    /**
     * @param string $returnId
     * @param string $itemId
     * @param ReturnIncomingItem $incomingItem
     * @return StockChange|null
     * @throws Throwable
     */
    public function incomingItem(string $returnId, string $itemId, ReturnIncomingItem $incomingItem): ?StockChange
    {
        try {
            $response = $this->getClient()->getHttp()->request(
                'POST',
                sprintf('fulfiller/returns/%s/items/%s/incoming-goods', $returnId, $itemId),
                [
                    'body' => json_encode(self::removeEmpty($incomingItem, $incomingItem->toArray()))
                ]
            );

            $this->getResourceCache()->delete($this->buildCacheKey($returnId . '_' . $itemId));

            // Delete Page Cache
            $this->deletePageCache();

            return new StockChange($this->extractData($response));
        } catch (Throwable $e) {
            HttpException::handleGuzzeException($e);
        }

        return null;
    }

    /**
     * @param string $returnId
     * @return bool
     * @throws Throwable
     */
    public function lock(string $returnId): bool
    {
        try {
            $response = $this->getClient()->getHttp()->request(
                'PUT',
                sprintf('fulfiller/returns/%s/lock', $returnId)
            );

            return $response->getStatusCode() === 204;
        } catch (Throwable $e) {
            HttpException::handleGuzzeException($e);
        }

        return false;
    }

    /**
     * @param string $returnId
     * @return bool
     * @throws Throwable
     */
    public function unlock(string $returnId): bool
    {
        try {
            $response = $this->getClient()->getHttp()->request(
                'PUT',
                sprintf('fulfiller/returns/%s/unlock', $returnId)
            );

            return $response->getStatusCode() === 204;
        } catch (Throwable $e) {
            HttpException::handleGuzzeException($e);
        }

        return false;
    }
}
