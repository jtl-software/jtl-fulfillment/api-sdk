<?php
namespace Jtl\Fulfillment\Api\Sdk\Resources\Fulfiller;

use Jtl\Fulfillment\Api\Sdk\Cache\NullCache;
use Throwable;
use Izzle\Model\Model;
use Jtl\Fulfillment\Api\Sdk\Client;
use Jtl\Fulfillment\Api\Sdk\Exceptions\JsonException;
use Jtl\Fulfillment\Api\Sdk\Models\General\Product\ProductStockLevel;
use Jtl\Fulfillment\Api\Sdk\Models\General\Product\ProductStockLevelWarehouse;
use Jtl\Fulfillment\Api\Sdk\Models\Fulfiller\Stock\StockAdjustment;
use \Jtl\Fulfillment\Api\Sdk\Models\Fulfiller\Stock\StockChange;
use Jtl\Fulfillment\Api\Sdk\Models\Pagination;
use Jtl\Fulfillment\Api\Sdk\Models\Query;
use Jtl\Fulfillment\Api\Sdk\Models\TimeFrame;
use Jtl\Fulfillment\Api\Sdk\Resources\Resource;

/**
 * Class StockResource
 * @package Jtl\Fulfillment\Api\Sdk\Resources
 */
class StockResource extends Resource
{
    /**
     * StockResource constructor.
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        parent::__construct($client);
        
        // Override global behavior and must be set, explizit on the resource controller
        $this->resourceCache->setCache(new NullCache());
    }
    
    /**
     * @param Query $query
     * @return Pagination
     * @throws Throwable
     */
    public function all(Query $query): Pagination
    {
        return $this->findAll(
            'fulfiller/stocks',
            ProductStockLevel::class,
            $query,
            $this->buildCacheKey((string) $query)
        );
    }
    
    /**
     * @param StockAdjustment|Model $model
     * @param Query|null $query
     * @return bool
     * @throws Throwable
     * @throws JsonException
     */
    public function save(Model $model, Query $query = null): bool
    {
        $this->validModel($model, StockAdjustment::class);
        
        /** @var StockAdjustment $model */
        return $this->create(
            $model,
            'fulfiller/stocks/adjustments'
        );
    }
    
    /**
     * @param string $jfsku
     * @return Model|ProductStockLevel|null
     * @throws Throwable
     * @throws JsonException
     */
    public function findBySku(string $jfsku): ?ProductStockLevel
    {
        return $this->findBy(
            sprintf('fulfiller/stocks/%s', $jfsku),
            ProductStockLevel::class,
            $this->buildCacheKey($jfsku)
        );
    }
    
    /**
     * @param string $jfsku
     * @param string $warehouseId
     * @return Model|ProductStockLevelWarehouse|null
     * @throws Throwable
     * @throws JsonException
     */
    public function findBySkuAndWarehouse(string $jfsku, string $warehouseId): ?ProductStockLevelWarehouse
    {
        return $this->findBy(
            sprintf(
                'fulfiller/stocks/%s/%s',
                $jfsku,
                $warehouseId
            ),
            ProductStockLevelWarehouse::class,
            $this->buildCacheKey($jfsku . $warehouseId)
        );
    }
    
    /**
     * @param string $warehouseId
     * @param Query $query
     * @return Pagination
     * @throws Throwable
     * @throws JsonException
     */
    public function findByWarehouse(string $warehouseId, Query $query): Pagination
    {
        return $this->findAll(
            sprintf('fulfiller/stocks/warehouse/%s', $warehouseId),
            ProductStockLevelWarehouse::class,
            $query,
            $this->buildCacheKey($warehouseId . $query)
        );
    }
    
    /**
     * @param Query $query
     * @return Pagination
     * @throws Throwable
     * @throws JsonException
     */
    public function allChanges(Query $query): Pagination
    {
        return $this->findAll(
            'fulfiller/stocks/changes',
            StockChange::class,
            $query,
            $this->buildCacheKey('fulfiller-changes-' . $query)
        );
    }
    
    /**
     * @param Query $query
     * @return TimeFrame
     * @throws Throwable
     * @throws JsonException
     */
    public function allUpdates(Query $query): TimeFrame
    {
        return $this->findUpdates(
            'fulfiller/stocks/updates',
            StockChange::class,
            $query,
            $this->buildCacheKey('fulfiller-updates-' . $query)
        );
    }
    
    /**
     * @param string $warehouseId
     * @param Query $query
     * @return Pagination
     * @throws Throwable
     * @throws JsonException
     */
    public function findChangesByWarehouse(string $warehouseId, Query $query): Pagination
    {
        return $this->findAll(
            sprintf('fulfiller/stocks/changes/%s', $warehouseId),
            StockChange::class,
            $query,
            $this->buildCacheKey('fulfiller-changes' . $warehouseId . $query)
        );
    }
}
