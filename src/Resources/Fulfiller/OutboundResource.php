<?php
namespace Jtl\Fulfillment\Api\Sdk\Resources\Fulfiller;

use Jtl\Fulfillment\Api\Sdk\Exceptions\HttpException;
use Throwable;
use Izzle\Model\Model;
use Jtl\Fulfillment\Api\Sdk\Exceptions\JsonException;
use Jtl\Fulfillment\Api\Sdk\Models\General\Attachment;
use Jtl\Fulfillment\Api\Sdk\Models\Fulfiller\Outbound\Outbound;
use Jtl\Fulfillment\Api\Sdk\Models\Fulfiller\Outbound\OutboundShippingNotification;
use Jtl\Fulfillment\Api\Sdk\Models\General\Outbound\OutboundShippingNotification as GeneralOutboundShippingNotification;
use Jtl\Fulfillment\Api\Sdk\Models\Pagination;
use Jtl\Fulfillment\Api\Sdk\Models\Query;
use Jtl\Fulfillment\Api\Sdk\Models\TimeFrame;
use Jtl\Fulfillment\Api\Sdk\Resources\Resource;

/**
 * Class OutboundResource
 * @package Jtl\Fulfillment\Api\Sdk\Resources\Fulfiller
 */
class OutboundResource extends Resource
{
    /**
     * @param Query $query
     * @return Pagination
     * @throws Throwable
     * @throws JsonException
     */
    public function all(Query $query): Pagination
    {
        return $this->findAll('fulfiller/outbounds', Outbound::class, $query, $this->buildCacheKey((string) $query));
    }
    
    /**
     * @param string $id
     * @param Query|null $query
     * @return Model|null
     * @throws Throwable
     * @throws JsonException
     */
    public function find(string $id, Query $query = null): ?Model
    {
        return $this->findBy(sprintf('fulfiller/outbounds/%s', $id), Outbound::class, $this->buildCacheKey($id), $query);
    }
    
    /**
     * @param Query $query
     * @return TimeFrame
     * @throws Throwable
     * @throws JsonException
     */
    public function findChangesByTimeFrame(Query $query): TimeFrame
    {
        return $this->findUpdates(
            'fulfiller/outbounds/updates',
            Outbound::class,
            $query
        );
    }
    
    /**
     * @param string $outboundId
     * @return Pagination
     * @throws Throwable
     * @throws JsonException
     */
    //public function findShippingNotifications(string $outboundId): array
    public function findShippingNotifications(string $outboundId): Pagination
    {
        $query = new Query();
    
        return $this->findAll(
            sprintf('fulfiller/outbounds/%s/shipping-notifications', $outboundId),
            OutboundShippingNotification::class,
            $query,
            $this->buildCacheKey((string) $query)
        );
        
        /*
        $result = [];

        try {
            $response = $this->getClient()->getHttp()->request(
                'GET',
                sprintf('fulfiller/outbounds/%s/shipping-notifications', $outboundId)
            );

            $data = $this->extractData($response);
            foreach ($data as $d) {
                $result[] = new OutboundShippingNotification($d);
            }
        } catch (Throwable $e) {
            HttpException::handleGuzzeException($e);
        }

        return $result;
        */
    }
    
    /**
     * @param string $outboundId
     * @param string $shippingNotificationNumber
     * @return OutboundShippingNotification|null
     * @throws Throwable
     * @throws JsonException
     */
    public function findShippingNotificationById(
        string $outboundId,
        string $shippingNotificationNumber
    ): ?OutboundShippingNotification {
        $cacheKey = $this->buildCacheKey($shippingNotificationNumber);
    
        // Try Cache
        $cachedItem = $this->getResourceCache()->get($cacheKey);
        if ($cachedItem !== null) {
            return $cachedItem;
        }
    
        try {
            $response = $this->getClient()->getHttp()->request(
                'GET',
                sprintf('fulfiller/outbounds/%s/shipping-notifications/%s', $outboundId, $shippingNotificationNumber)
            );
    
            $notification = new OutboundShippingNotification($this->extractData($response));
    
            // Set Cache
            $this->getResourceCache()->set($notification, $cacheKey);
    
            return $notification;
        } catch (Throwable $e) {
            HttpException::handleGuzzeException($e);
        }
        
        return null;
    }
    
    /**
     * @param string $outboundId
     * @param string $documentId
     * @return Attachment|null
     * @throws Throwable
     * @throws JsonException
     */
    public function findAttachment(string $outboundId, string $documentId): ?Attachment
    {
        try {
            $response = $this->getClient()->getHttp()->request(
                'GET',
                sprintf('fulfiller/outbounds/%s/attachments/%s', $outboundId, $documentId)
            );
        
            return new Attachment($this->extractData($response));
        } catch (Throwable $e) {
            HttpException::handleGuzzeException($e);
        }
    
        return null;
    }
    
    /**
     * @param string $outboundId
     * @param string $status
     * @return bool
     * @throws Throwable
     */
    public function updateStatus(string $outboundId, string $status): bool
    {
        try {
            $response = $this->getClient()->getHttp()->request(
                'PUT',
                sprintf('fulfiller/outbounds/%s/status', $outboundId),
                [
                    'body' => json_encode(['status' => $status])
                ]
            );
        
            $result = $response->getStatusCode() === 204;
            if ($result) {
                $this->getResourceCache()->delete($this->buildCacheKey($outboundId));
                
                // Delete Page Cache
                $this->deletePageCache();
            }
            
            return $result;
        } catch (Throwable $e) {
            HttpException::handleGuzzeException($e);
        }
    
        return false;
    }
    
    // @TODO: Change Status of multiple outbounds
    /*
     * [
     *     {
     *         "cancelReason": "string",
     *         "cancelReasonCode": "string",
     *         "outboundId": "MERC02OUTBND01",
     *         "status": "Preparation"
     *     }
     * ]
     */
    // public function updateStatuses(array $data): bool
    
    /**
     * @param string $outboundId
     * @param string $warehouseId
     * @return bool
     * @throws Throwable
     */
    public function changeWarehouse(string $outboundId, string $warehouseId): bool
    {
        try {
            $response = $this->getClient()->getHttp()->request(
                'PUT',
                sprintf('fulfiller/outbounds/%s/warehouse', $outboundId),
                [
                    'body' => json_encode(['warehouseId' => $warehouseId])
                ]
            );
    
            $result = $response->getStatusCode() === 204;
            if ($result) {
                $this->getResourceCache()->delete($this->buildCacheKey($outboundId));
        
                // Delete Page Cache
                $this->deletePageCache();
            }
        
            return $result;
        } catch (Throwable $e) {
            HttpException::handleGuzzeException($e);
        }
    
        return false;
    }
    
    /**
     * @param string $outboundId
     * @param GeneralOutboundShippingNotification $notification
     * @return bool
     * @throws Throwable
     */
    public function declareShipped(string $outboundId, GeneralOutboundShippingNotification $notification): bool
    {
        try {
            $response = $this->getClient()->getHttp()->request(
                'POST',
                sprintf('fulfiller/outbounds/%s/shipping-notifications', $outboundId),
                [
                    'body' => json_encode($notification)
                ]
            );
            
            $result = $response->getStatusCode() === 201;
            if ($result) {
                $this->getResourceCache()->delete($this->buildCacheKey($outboundId));
                
                // Delete Page Cache
                $this->deletePageCache();
            }
            
            return $result;
        } catch (Throwable $e) {
            HttpException::handleGuzzeException($e);
        }
        
        return false;
    }
}
