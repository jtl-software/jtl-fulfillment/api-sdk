<?php
namespace Jtl\Fulfillment\Api\Sdk\Models\General\Inbound;

use Jtl\Fulfillment\Api\Sdk\Models\DataModel;
use Izzle\Model\PropertyCollection;
use Izzle\Model\PropertyInfo;
use Jtl\Fulfillment\Api\Sdk\Models\General\Attribute;
use Jtl\Fulfillment\Api\Sdk\Models\General\ModificationInfo;

/**
 * Class Inbound
 * @package Jtl\Fulfillment\Api\Sdk\Models\General\Inbound
 */
class Inbound extends DataModel
{
    public const STATUS_PREPARATION = 'Preparation';
    public const STATUS_PENDING = 'Pending';
    public const STATUS_PARTIALLYRECEIPTED = 'PartiallyReceipted';
    public const STATUS_RECEIPTED = 'Receipted';
    public const STATUS_CLOSED = 'Closed';
    
    /**
     * @var string|null
     */
    protected $inboundId;
    
    /**
     * @var InboundItem[]
     */
    protected $items = [];
    
    /**
     * @var ModificationInfo|null
     */
    protected $modificationInfo;
    
    /**
     * @var string|null
     */
    protected $status = self::STATUS_PENDING;
    
    /**
     * @var string|null
     */
    protected $merchantInboundNumber;
    
    /**
     * @var string|null
     */
    protected $warehouseId;
    
    /**
     * @var string|null
     */
    protected $note;
    
    /**
     * @var string|null
     */
    protected $purchaseOrderNumber;
    
    /**
     * @var string|null
     */
    protected $externalInboundNumber;
    
    /**
     * @var Attribute[]
     */
    protected $attributes = [];
    
    /**
     * @return string|null
     */
    public function getInboundId(): ?string
    {
        return $this->inboundId;
    }
    
    /**
     * @param string|null $inboundId
     * @return \Jtl\Fulfillment\Api\Sdk\Models\Fulfiller\Inbound\Inbound
     */
    public function setInboundId(?string $inboundId): Inbound
    {
        $this->inboundId = $inboundId;
        
        return $this;
    }
    
    /**
     * @return InboundItem[]
     */
    public function getItems(): array
    {
        return $this->items;
    }
    
    /**
     * @param InboundItem[] $items
     * @return Inbound
     */
    public function setItems(array $items): Inbound
    {
        $this->items = $items;
        
        return $this;
    }
    
    /**
     * @param InboundItem $item
     * @param string|null $key
     * @return Inbound
     */
    public function addItem(InboundItem $item, string $key = null): Inbound
    {
        if ($key === null) {
            $this->items[] = $item;
        } else {
            $this->items[$key] = $item;
        }
        
        return $this;
    }
    
    /**
     * @return ModificationInfo|null
     */
    public function getModificationInfo(): ?ModificationInfo
    {
        return $this->modificationInfo;
    }
    
    /**
     * @param ModificationInfo|null $modificationInfo
     * @return Inbound
     */
    public function setModificationInfo(?ModificationInfo $modificationInfo): Inbound
    {
        $this->modificationInfo = $modificationInfo;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getStatus(): ?string
    {
        return $this->status;
    }
    
    /**
     * @param string|null $status
     * @return Inbound
     */
    public function setStatus(?string $status): Inbound
    {
        $this->status = $status;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getMerchantInboundNumber(): ?string
    {
        return $this->merchantInboundNumber;
    }
    
    /**
     * @param string|null $merchantInboundNumber
     * @return Inbound
     */
    public function setMerchantInboundNumber(?string $merchantInboundNumber): Inbound
    {
        $this->merchantInboundNumber = $merchantInboundNumber;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getWarehouseId(): ?string
    {
        return $this->warehouseId;
    }
    
    /**
     * @param string|null $warehouseId
     * @return Inbound
     */
    public function setWarehouseId(?string $warehouseId): Inbound
    {
        $this->warehouseId = $warehouseId;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getNote(): ?string
    {
        return $this->note;
    }
    
    /**
     * @param string|null $note
     * @return Inbound
     */
    public function setNote(?string $note): Inbound
    {
        $this->note = $note;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getPurchaseOrderNumber(): ?string
    {
        return $this->purchaseOrderNumber;
    }
    
    /**
     * @param string|null $purchaseOrderNumber
     * @return Inbound
     */
    public function setPurchaseOrderNumber(?string $purchaseOrderNumber): Inbound
    {
        $this->purchaseOrderNumber = $purchaseOrderNumber;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getExternalInboundNumber(): ?string
    {
        return $this->externalInboundNumber;
    }
    
    /**
     * @param string|null $externalInboundNumber
     * @return Inbound
     */
    public function setExternalInboundNumber(?string $externalInboundNumber): Inbound
    {
        $this->externalInboundNumber = $externalInboundNumber;
        
        return $this;
    }
    
    /**
     * @return Attribute[]
     */
    public function getAttributes(): array
    {
        return $this->attributes;
    }
    
    /**
     * @param Attribute[] $attributes
     * @return Inbound
     */
    public function setAttributes(array $attributes): Inbound
    {
        $this->attributes = $attributes;
        
        return $this;
    }
    
    /**
     * @param Attribute $attribute
     * @param string|null $key
     * @return Inbound
     */
    public function addAttribute(Attribute $attribute, string $key = null): Inbound
    {
        if ($key === null) {
            $this->attributes[] = $attribute;
        } else {
            $this->attributes[$key] = $attribute;
        }
        
        return $this;
    }
    
    /**
     * @return PropertyCollection
     */
    protected function loadProperties(): PropertyCollection
    {
        return new PropertyCollection([
            new PropertyInfo('inboundId', 'string', null),
            new PropertyInfo('items', InboundItem::class, [], true, true),
            new PropertyInfo('modificationInfo', ModificationInfo::class, null, true),
            new PropertyInfo('status', 'string', self::STATUS_PENDING),
            new PropertyInfo('merchantInboundNumber', 'string', null),
            new PropertyInfo('warehouseId', 'string', null),
            new PropertyInfo('note', 'string', null),
            new PropertyInfo('purchaseOrderNumber', 'string', null),
            new PropertyInfo('externalInboundNumber', 'string', null),
            new PropertyInfo('attributes', Attribute::class, [], true, true)
        ]);
    }
}
