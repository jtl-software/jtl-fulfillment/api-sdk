<?php
namespace Jtl\Fulfillment\Api\Sdk\Models\General\Product;

use Jtl\Fulfillment\Api\Sdk\Models\DataModel;
use Izzle\Model\PropertyCollection;
use Izzle\Model\PropertyInfo;

/**
 * Class ProductPrice
 * @package Jtl\Fulfillment\Api\Sdk\Models\General\Product
 */
class ProductPrice extends DataModel
{
    /**
     * @var float|null
     */
    protected $amount;
    
    /**
     * @var string|null
     */
    protected $currency;
    
    /**
     * @return float|null
     */
    public function getAmount(): ?float
    {
        return $this->amount;
    }
    
    /**
     * @param float|null $amount
     * @return ProductPrice
     */
    public function setAmount(?float $amount): ProductPrice
    {
        $this->amount = $amount;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getCurrency(): ?string
    {
        return $this->currency;
    }
    
    /**
     * @param string|null $currency
     * @return ProductPrice
     */
    public function setCurrency(?string $currency): ProductPrice
    {
        $this->currency = $currency;
        
        return $this;
    }
    
    /**
     * @return PropertyCollection
     */
    protected function loadProperties(): PropertyCollection
    {
        return new PropertyCollection([
            new PropertyInfo('amount', 'float', null),
            new PropertyInfo('currency', 'string', null)
        ]);
    }
}
