<?php
namespace Jtl\Fulfillment\Api\Sdk\Models\General\Product;

use Izzle\Model\PropertyCollection;
use Izzle\Model\PropertyInfo;
use Jtl\Fulfillment\Api\Sdk\Models\General\Stock\StockLevel;

/**
 * Class ProductStockLevelWarehouse
 * @package Jtl\Fulfillment\Api\Sdk\Models\Fulfiller\Product
 */
class ProductStockLevelWarehouse extends StockLevel
{
    /**
     * @var string|null
     */
    protected $warehouseId;
    
    /**
     * @return string|null
     */
    public function getWarehouseId(): ?string
    {
        return $this->warehouseId;
    }
    
    /**
     * @param string|null $warehouseId
     * @return ProductStockLevelWarehouse
     */
    public function setWarehouseId(?string $warehouseId): ProductStockLevelWarehouse
    {
        $this->warehouseId = $warehouseId;
        
        return $this;
    }
    
    /**
     * @return PropertyCollection
     */
    protected function loadProperties(): PropertyCollection
    {
        return parent::loadProperties()->addProperty(
            new PropertyInfo('warehouseId', 'string', null)
        );
    }
}
