<?php
namespace Jtl\Fulfillment\Api\Sdk\Models\General\Product;

use Jtl\Fulfillment\Api\Sdk\Models\DataModel;
use Izzle\Model\PropertyCollection;
use Izzle\Model\PropertyInfo;

/**
 * Class ProductSpecification
 * @package Jtl\Fulfillment\Api\Sdk\Models\General\Product
 */
class ProductSpecification extends DataModel
{
    /**
     * @var string|null
     */
    protected $fnsku;
    
    /**
     * @var string|null
     */
    protected $unNumber;
    
    /**
     * @var string|null
     */
    protected $hazardIdentifier;
    
    /**
     * @var string|null
     */
    protected $taric;
    
    /**
     * @var bool|null
     */
    protected $isBatch = false;
    
    /**
     * @var bool|null
     */
    protected $isDivisible = false;
    
    /**
     * @var bool|null
     */
    protected $isBestBefore = false;
    
    /**
     * @var bool|null
     */
    protected $isSerialNumber = false;

    /**
     * @var bool|null
     */
    protected $isPackaging = false;
    
    /**
     * @var bool|null
     */
    protected $isBillOfMaterials = false;
    
    /**
     * @var ProductBillOfMaterialsComponent[]
     */
    protected $billOfMaterialsComponents = [];
    
    /**
     * @return string|null
     */
    public function getFnsku(): ?string
    {
        return $this->fnsku;
    }
    
    /**
     * @param string|null $fnsku
     * @return ProductSpecification
     */
    public function setFnsku(?string $fnsku): ProductSpecification
    {
        $this->fnsku = $fnsku;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getUnNumber(): ?string
    {
        return $this->unNumber;
    }
    
    /**
     * @param string|null $unNumber
     * @return ProductSpecification
     */
    public function setUnNumber(?string $unNumber): ProductSpecification
    {
        $this->unNumber = $unNumber;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getHazardIdentifier(): ?string
    {
        return $this->hazardIdentifier;
    }
    
    /**
     * @param string|null $hazardIdentifier
     * @return ProductSpecification
     */
    public function setHazardIdentifier(?string $hazardIdentifier): ProductSpecification
    {
        $this->hazardIdentifier = $hazardIdentifier;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getTaric(): ?string
    {
        return $this->taric;
    }
    
    /**
     * @param string|null $taric
     * @return ProductSpecification
     */
    public function setTaric(?string $taric): ProductSpecification
    {
        $this->taric = $taric;
        
        return $this;
    }
    
    /**
     * @return bool|null
     */
    public function isBatch(): ?bool
    {
        return $this->isBatch;
    }
    
    /**
     * @param bool|null $isBatch
     * @return ProductSpecification
     */
    public function setIsBatch(?bool $isBatch): ProductSpecification
    {
        $this->isBatch = $isBatch;
        
        return $this;
    }
    
    /**
     * @return bool|null
     */
    public function isDivisible(): ?bool
    {
        return $this->isDivisible;
    }
    
    /**
     * @param bool|null $isDivisible
     * @return ProductSpecification
     */
    public function setIsDivisible(?bool $isDivisible): ProductSpecification
    {
        $this->isDivisible = $isDivisible;
        
        return $this;
    }
    
    /**
     * @return bool|null
     */
    public function isBestBefore(): ?bool
    {
        return $this->isBestBefore;
    }
    
    /**
     * @param bool|null $isBestBefore
     * @return ProductSpecification
     */
    public function setIsBestBefore(?bool $isBestBefore): ProductSpecification
    {
        $this->isBestBefore = $isBestBefore;
        
        return $this;
    }
    
    /**
     * @return bool|null
     */
    public function isSerialNumber(): ?bool
    {
        return $this->isSerialNumber;
    }
    
    /**
     * @param bool|null $isSerialNumber
     * @return ProductSpecification
     */
    public function setIsSerialNumber(?bool $isSerialNumber): ProductSpecification
    {
        $this->isSerialNumber = $isSerialNumber;
        
        return $this;
    }

    /**
     * @return bool|null
     */
    public function isPackaging(): ?bool
    {
        return $this->isPackaging;
    }

    /**
     * @param bool|null $isPackaging
     * @return ProductSpecification
     */
    public function setIsPackaging(?bool $isPackaging): ProductSpecification
    {
        $this->isPackaging = $isPackaging;

        return $this;
    }
    
    /**
     * @return bool|null
     */
    public function isBillOfMaterials(): ?bool
    {
        return $this->isBillOfMaterials;
    }
    
    /**
     * @param bool|null $isBillOfMaterials
     * @return ProductSpecification
     */
    public function setIsBillOfMaterials(?bool $isBillOfMaterials): ProductSpecification
    {
        $this->isBillOfMaterials = $isBillOfMaterials;
        
        return $this;
    }
    
    /**
     * @return ProductBillOfMaterialsComponent[]
     */
    public function getBillOfMaterialsComponents(): array
    {
        return $this->billOfMaterialsComponents;
    }
    
    /**
     * @param ProductBillOfMaterialsComponent[] $billOfMaterialsComponents
     * @return ProductSpecification
     */
    public function setBillOfMaterialsComponents(array $billOfMaterialsComponents): ProductSpecification
    {
        $this->billOfMaterialsComponents = $billOfMaterialsComponents;
        
        return $this;
    }
    
    /**
     * @param ProductBillOfMaterialsComponent $component
     * @param string|null $key
     * @return ProductSpecification
     */
    public function addBillOfMaterialsComponent(ProductBillOfMaterialsComponent $component, ?string $key = null): ProductSpecification
    {
        if ($key === null) {
            $this->billOfMaterialsComponents[] = $component;
        } else {
            $this->billOfMaterialsComponents[$key] = $component;
        }
        
        return $this;
    }
    
    /**
     * @return PropertyCollection
     */
    protected function loadProperties(): PropertyCollection
    {
        return new PropertyCollection([
            new PropertyInfo('fnsku', 'string', null),
            new PropertyInfo('unNumber', 'string', null),
            new PropertyInfo('hazardIdentifier', 'string', null),
            new PropertyInfo('taric', 'string', null),
            new PropertyInfo('isBatch', 'bool', false),
            new PropertyInfo('isDivisible', 'bool', false),
            new PropertyInfo('isBestBefore', 'bool', false),
            new PropertyInfo('isSerialNumber', 'bool', false),
            new PropertyInfo('isPackaging', 'bool', false),
            new PropertyInfo('isBillOfMaterials', 'bool', false),
            new PropertyInfo('billOfMaterialsComponents', ProductBillOfMaterialsComponent::class, [], true, true)
        ]);
    }
}
