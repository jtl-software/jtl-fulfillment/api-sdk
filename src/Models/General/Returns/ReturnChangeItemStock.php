<?php
namespace Jtl\Fulfillment\Api\Sdk\Models\General\Returns;

use Izzle\Model\PropertyCollection;
use Izzle\Model\PropertyInfo;
use Jtl\Fulfillment\Api\Sdk\Models\DataModel;
use Jtl\Fulfillment\Api\Sdk\Models\General\ChangeValue;
use Jtl\Fulfillment\Api\Sdk\Models\General\ChangeValueFloat;

/**
 * Class ReturnChangeItemStock
 * @package Jtl\Fulfillment\Api\Sdk\Models\General\Returns
 */
class ReturnChangeItemStock extends DataModel
{
    /**
     * @var ReturnStockChangeIdChange|null
     */
    protected $stockChangeId;

    /**
     * @var ChangeValue|null
     */
    protected $merchantSku;

    /**
     * @var ChangeValueFloat|null
     */
    protected $quantity;

    /**
     * @var ChangeValueFloat|null
     */
    protected $quantityBlocked;

    /**
     * @return ReturnStockChangeIdChange|null
     */
    public function getStockChangeId(): ?ReturnStockChangeIdChange
    {
        return $this->stockChangeId;
    }

    /**
     * @param ReturnStockChangeIdChange|null $stockChangeId
     * @return ReturnChangeItemStock
     */
    public function setStockChangeId(?ReturnStockChangeIdChange $stockChangeId): ReturnChangeItemStock
    {
        $this->stockChangeId = $stockChangeId;
        return $this;
    }

    /**
     * @return ChangeValue|null
     */
    public function getMerchantSku(): ?ChangeValue
    {
        return $this->merchantSku;
    }

    /**
     * @param ChangeValue|null $merchantSku
     * @return ReturnChangeItemStock
     */
    public function setMerchantSku(?ChangeValue $merchantSku): ReturnChangeItemStock
    {
        $this->merchantSku = $merchantSku;
        return $this;
    }

    /**
     * @return ChangeValueFloat|null
     */
    public function getQuantity(): ?ChangeValueFloat
    {
        return $this->quantity;
    }

    /**
     * @param ChangeValueFloat|null $quantity
     * @return ReturnChangeItemStock
     */
    public function setQuantity(?ChangeValueFloat $quantity): ReturnChangeItemStock
    {
        $this->quantity = $quantity;
        return $this;
    }

    /**
     * @return ChangeValueFloat|null
     */
    public function getQuantityBlocked(): ?ChangeValueFloat
    {
        return $this->quantityBlocked;
    }

    /**
     * @param ChangeValueFloat|null $quantityBlocked
     * @return ReturnChangeItemStock
     */
    public function setQuantityBlocked(?ChangeValueFloat $quantityBlocked): ReturnChangeItemStock
    {
        $this->quantityBlocked = $quantityBlocked;
        return $this;
    }

    /**
     * @return PropertyCollection
     */
    protected function loadProperties(): PropertyCollection
    {
        return new PropertyCollection([
            new PropertyInfo('stockChangeId', ReturnStockChangeIdChange::class, null, true),
            new PropertyInfo('merchantSku', ChangeValue::class, null, true),
            new PropertyInfo('quantity', ChangeValueFloat::class, null, true),
            new PropertyInfo('quantityBlocked', ChangeValueFloat::class, null, true),
        ]);
    }
}
