<?php
namespace Jtl\Fulfillment\Api\Sdk\Models\General\Returns;

use Izzle\Model\PropertyCollection;
use Izzle\Model\PropertyInfo;
use Jtl\Fulfillment\Api\Sdk\Models\DataModel;
use Jtl\Fulfillment\Api\Sdk\Models\General\Address;
use Jtl\Fulfillment\Api\Sdk\Models\General\ModificationInfo;

/**
 * Class BaseReturn
 * @package Jtl\Fulfillment\Api\Sdk\Models\General\Returns
 */
class BaseReturn extends DataModel
{
    /**
     * @var string|null
     */
    protected $returnId;

    /**
     * @var string|null
     */
    protected $warehouseId;

    /**
     * @var string|null
     */
    protected $internalNote;

    /**
     * @var string|null
     */
    protected $externalNote;

    /**
     * @var string|null
     */
    protected $merchantReturnNumber;

    /**
     * @var string|null
     */
    protected $fulfillerReturnNumber;

    /**
     * @var string|null
     */
    protected $state;

    /**
     * @var string|null
     */
    protected $contact;

    /**
     * @var Address|null
     */
    protected $customerAddress;

    /**
     * @var ModificationInfo|null
     */
    protected $modificationInfo;

    /**
     * @var WriteLock|null
     */
    protected $writeLock;

    /**
     * @var ReturnItem[]
     */
    protected $items = [];

    /**
     * @return string|null
     */
    public function getReturnId(): ?string
    {
        return $this->returnId;
    }

    /**
     * @param string|null $returnId
     * @return BaseReturn
     */
    public function setReturnId(?string $returnId): BaseReturn
    {
        $this->returnId = $returnId;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getWarehouseId(): ?string
    {
        return $this->warehouseId;
    }

    /**
     * @param string|null $warehouseId
     * @return BaseReturn
     */
    public function setWarehouseId(?string $warehouseId): BaseReturn
    {
        $this->warehouseId = $warehouseId;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getInternalNote(): ?string
    {
        return $this->internalNote;
    }

    /**
     * @param string|null $internalNote
     * @return BaseReturn
     */
    public function setInternalNote(?string $internalNote): BaseReturn
    {
        $this->internalNote = $internalNote;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getExternalNote(): ?string
    {
        return $this->externalNote;
    }

    /**
     * @param string|null $externalNote
     * @return BaseReturn
     */
    public function setExternalNote(?string $externalNote): BaseReturn
    {
        $this->externalNote = $externalNote;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getMerchantReturnNumber(): ?string
    {
        return $this->merchantReturnNumber;
    }

    /**
     * @param string|null $merchantReturnNumber
     * @return BaseReturn
     */
    public function setMerchantReturnNumber(?string $merchantReturnNumber): BaseReturn
    {
        $this->merchantReturnNumber = $merchantReturnNumber;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getFulfillerReturnNumber(): ?string
    {
        return $this->fulfillerReturnNumber;
    }

    /**
     * @param string|null $fulfillerReturnNumber
     * @return BaseReturn
     */
    public function setFulfillerReturnNumber(?string $fulfillerReturnNumber): BaseReturn
    {
        $this->fulfillerReturnNumber = $fulfillerReturnNumber;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getState(): ?string
    {
        return $this->state;
    }

    /**
     * @param string|null $state
     * @return BaseReturn
     */
    public function setState(?string $state): BaseReturn
    {
        $this->state = $state;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getContact(): ?string
    {
        return $this->contact;
    }

    /**
     * @param string|null $contact
     * @return BaseReturn
     */
    public function setContact(?string $contact): BaseReturn
    {
        $this->contact = $contact;
        return $this;
    }

    /**
     * @return Address|null
     */
    public function getCustomerAddress(): ?Address
    {
        return $this->customerAddress;
    }

    /**
     * @param Address|null $customerAddress
     * @return BaseReturn
     */
    public function setCustomerAddress(?Address $customerAddress): BaseReturn
    {
        $this->customerAddress = $customerAddress;
        return $this;
    }

    /**
     * @return ModificationInfo|null
     */
    public function getModificationInfo(): ?ModificationInfo
    {
        return $this->modificationInfo;
    }

    /**
     * @param ModificationInfo|null $modificationInfo
     * @return BaseReturn
     */
    public function setModificationInfo(?ModificationInfo $modificationInfo): BaseReturn
    {
        $this->modificationInfo = $modificationInfo;
        return $this;
    }

    /**
     * @return WriteLock|null
     */
    public function getWriteLock(): ?WriteLock
    {
        return $this->writeLock;
    }

    /**
     * @param WriteLock|null $writeLock
     * @return BaseReturn
     */
    public function setWriteLock(?WriteLock $writeLock): BaseReturn
    {
        $this->writeLock = $writeLock;
        return $this;
    }

    /**
     * @return ReturnItem[]
     */
    public function getItems(): array
    {
        return $this->items;
    }

    /**
     * @param ReturnItem[] $items
     * @return BaseReturn
     */
    public function setItems(array $items): BaseReturn
    {
        $this->items = $items;
        return $this;
    }

    /**
     * @param ReturnItem $item
     * @param string|null $key
     * @return BaseReturn
     */
    public function addItem(ReturnItem $item, string $key = null): BaseReturn
    {
        if ($key === null) {
            $this->items[] = $item;
        } else {
            $this->items[$key] = $item;
        }

        return $this;
    }

    /**
     * @return PropertyCollection
     */
    protected function loadProperties(): PropertyCollection
    {
        return new PropertyCollection([
            new PropertyInfo('returnId', 'string', null),
            new PropertyInfo('warehouseId', 'string', null),
            new PropertyInfo('internalNote', 'string', null),
            new PropertyInfo('externalNote', 'string', null),
            new PropertyInfo('contact', 'string', null),
            new PropertyInfo('merchantReturnNumber', 'string', null),
            new PropertyInfo('fulfillerReturnNumber', 'string', null),
            new PropertyInfo('customerAddress', Address::class, null, true),
            new PropertyInfo('state', 'string', null),
            new PropertyInfo('modificationInfo', ModificationInfo::class, null, true),
            new PropertyInfo('writeLock', WriteLock::class, null, true),
            new PropertyInfo('items', ReturnItem::class, [], true, true),
        ]);
    }
}
