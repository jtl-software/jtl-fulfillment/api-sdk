<?php
namespace Jtl\Fulfillment\Api\Sdk\Models\General\Returns;

use Izzle\Model\PropertyCollection;
use Izzle\Model\PropertyInfo;
use Jtl\Fulfillment\Api\Sdk\Models\DataModel;
use DateTime;
use DateTimeZone;
use Exception;

/**
 * Class ReturnChangeItemStockShell
 * @package Jtl\Fulfillment\Api\Sdk\Models\General\Returns
 */
class ReturnChangeItemStockShell extends DataModel
{
    /**
     * @var ReturnStockChangeId|null
     */
    protected $id;

    /**
     * @var string|null
     */
    protected $modificationState;

    /**
     * @var string|null
     */
    protected $modifiedFrom;

    /**
     * @var DateTime|null
     */
    protected $updatedAt;

    /**
     * @var ReturnChangeItemStock|null
     */
    protected $changes;

    /**
     * @return ReturnStockChangeId|null
     */
    public function getId(): ?ReturnStockChangeId
    {
        return $this->id;
    }

    /**
     * @param ReturnStockChangeId|null $id
     * @return ReturnChangeItemStockShell
     */
    public function setId(?ReturnStockChangeId $id): ReturnChangeItemStockShell
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getModificationState(): ?string
    {
        return $this->modificationState;
    }

    /**
     * @param string|null $modificationState
     * @return ReturnChangeItemStockShell
     */
    public function setModificationState(?string $modificationState): ReturnChangeItemStockShell
    {
        $this->modificationState = $modificationState;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getModifiedFrom(): ?string
    {
        return $this->modifiedFrom;
    }

    /**
     * @param string|null $modifiedFrom
     * @return ReturnChangeItemStockShell
     */
    public function setModifiedFrom(?string $modifiedFrom): ReturnChangeItemStockShell
    {
        $this->modifiedFrom = $modifiedFrom;
        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getUpdatedAt(): ?DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @param DateTime|string|null $updatedAt
     * @return ReturnChangeItemStockShell
     * @throws Exception
     */
    public function setUpdatedAt($updatedAt): ReturnChangeItemStockShell
    {
        if ($updatedAt === null) {
            return $this;
        }

        if (is_string($updatedAt)) {
            $updatedAt = (new DateTime($updatedAt))->setTimezone(new DateTimeZone('UTC'));
        }

        $this->checkDate($updatedAt);
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @return ReturnChangeItemStock|null
     */
    public function getChanges(): ?ReturnChangeItemStock
    {
        return $this->changes;
    }

    /**
     * @param ReturnChangeItemStock|null $changes
     * @return ReturnChangeItemStockShell
     */
    public function setChanges(?ReturnChangeItemStock $changes): ReturnChangeItemStockShell
    {
        $this->changes = $changes;
        return $this;
    }

    /**
     * @return PropertyCollection
     */
    protected function loadProperties(): PropertyCollection
    {
        return new PropertyCollection([
            new PropertyInfo('id', ReturnStockChangeId::class, null, true),
            new PropertyInfo('modificationState', 'string', null),
            new PropertyInfo('modifiedFrom', 'string', null),
            new PropertyInfo('updatedAt', DateTime::class, null),
            new PropertyInfo('changes', ReturnChangeItemStock::class, null, true),
        ]);
    }
}
