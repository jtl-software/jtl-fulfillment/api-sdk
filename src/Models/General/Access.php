<?php
namespace Jtl\Fulfillment\Api\Sdk\Models\General;

use Jtl\Fulfillment\Api\Sdk\Models\DataModel;
use Izzle\Model\PropertyCollection;
use Izzle\Model\PropertyInfo;

/**
 * Class Access
 * @package Jtl\Fulfillment\Api\Sdk\Models\General
 */
class Access extends DataModel
{
    /**
     * @var string|null
     */
    protected $applicationId;
    
    /**
     * @return string|null
     */
    public function getApplicationId(): ?string
    {
        return $this->applicationId;
    }
    
    /**
     * @param string|null $applicationId
     * @return Access
     */
    public function setApplicationId(?string $applicationId): Access
    {
        $this->applicationId = $applicationId;
        
        return $this;
    }
    
    /**
     * @return PropertyCollection
     */
    protected function loadProperties(): PropertyCollection
    {
        return new PropertyCollection([
            new PropertyInfo('applicationId', 'string', null)
        ]);
    }
}
