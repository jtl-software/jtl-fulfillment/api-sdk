<?php
namespace Jtl\Fulfillment\Api\Sdk\Models\General\Outbound;

use Exception;
use Jtl\Fulfillment\Api\Sdk\Models\DataModel;
use Izzle\Model\PropertyCollection;
use DateTime;
use DateTimeZone;
use Izzle\Model\PropertyInfo;

/**
 * Class OutboundStatusTimestamp
 * @package Jtl\Fulfillment\Api\Sdk\Models\General\Outbound
 */
class OutboundStatusTimestamp extends DataModel
{
    /**
     * @var DateTime|null
     */
    protected $pending;

    /**
     * @var DateTime|null
     */
    protected $preparation;

    /**
     * @var DateTime|null
     */
    protected $acknowledged;

    /**
     * @var DateTime|null
     */
    protected $locked;

    /**
     * @var DateTime|null
     */
    protected $pickprocess;

    /**
     * @var DateTime|null
     */
    protected $shipped;

    /**
     * @var DateTime|null
     */
    protected $partiallyShipped;

    /**
     * @var DateTime|null
     */
    protected $canceled;

    /**
     * @var DateTime|null
     */
    protected $partiallyCanceled;

    /**
     * @return DateTime|null
     */
    public function getPending(): ?DateTime
    {
        return $this->pending;
    }

    /**
     * @param DateTime|string|null $pending
     * @return OutboundStatusTimestamp
     * @throws Exception
     */
    public function setPending($pending): OutboundStatusTimestamp
    {
        return $this->setDateTime($pending, 'pending');
    }

    /**
     * @return DateTime|null
     */
    public function getPreparation(): ?DateTime
    {
        return $this->preparation;
    }

    /**
     * @param DateTime|string|null $preparation
     * @return OutboundStatusTimestamp
     * @throws Exception
     */
    public function setPreparation($preparation): OutboundStatusTimestamp
    {
        return $this->setDateTime($preparation, 'preparation');
    }

    /**
     * @return DateTime|null
     */
    public function getAcknowledged(): ?DateTime
    {
        return $this->acknowledged;
    }

    /**
     * @param DateTime|string|null $acknowledged
     * @return OutboundStatusTimestamp
     * @throws Exception
     */
    public function setAcknowledged($acknowledged): OutboundStatusTimestamp
    {
        return $this->setDateTime($acknowledged, 'acknowledged');
    }

    /**
     * @return DateTime|null
     */
    public function getLocked(): ?DateTime
    {
        return $this->locked;
    }

    /**
     * @param DateTime|string|null $locked
     * @return OutboundStatusTimestamp
     * @throws Exception
     */
    public function setLocked($locked): OutboundStatusTimestamp
    {
        return $this->setDateTime($locked, 'locked');
    }

    /**
     * @return DateTime|null
     */
    public function getPickprocess(): ?DateTime
    {
        return $this->pickprocess;
    }

    /**
     * @param DateTime|string|null $pickprocess
     * @return OutboundStatusTimestamp
     * @throws Exception
     */
    public function setPickprocess($pickprocess): OutboundStatusTimestamp
    {
        return $this->setDateTime($pickprocess, 'pickprocess');
    }

    /**
     * @return DateTime|null
     */
    public function getShipped(): ?DateTime
    {
        return $this->shipped;
    }

    /**
     * @param DateTime|string|null $shipped
     * @return OutboundStatusTimestamp
     * @throws Exception
     */
    public function setShipped($shipped): OutboundStatusTimestamp
    {
        return $this->setDateTime($shipped, 'shipped');
    }

    /**
     * @return DateTime|null
     */
    public function getPartiallyShipped(): ?DateTime
    {
        return $this->partiallyShipped;
    }

    /**
     * @param DateTime|string|null $partiallyShipped
     * @return OutboundStatusTimestamp
     * @throws Exception
     */
    public function setPartiallyShipped($partiallyShipped): OutboundStatusTimestamp
    {
        return $this->setDateTime($partiallyShipped, 'partiallyShipped');
    }

    /**
     * @return DateTime|null
     */
    public function getCanceled(): ?DateTime
    {
        return $this->canceled;
    }

    /**
     * @param DateTime|string|null $canceled
     * @return OutboundStatusTimestamp
     * @throws Exception
     */
    public function setCanceled($canceled): OutboundStatusTimestamp
    {
        return $this->setDateTime($canceled, 'canceled');
    }

    /**
     * @return DateTime|null
     */
    public function getPartiallyCanceled(): ?DateTime
    {
        return $this->partiallyCanceled;
    }

    /**
     * @param DateTime|string|null $partiallyCanceled
     * @return OutboundStatusTimestamp
     * @throws Exception
     */
    public function setPartiallyCanceled($partiallyCanceled): OutboundStatusTimestamp
    {
        return $this->setDateTime($partiallyCanceled, 'partiallyCanceled');
    }

    /**
     * @param mixed $dateTime
     * @param string $property
     * @return $this
     * @throws Exception
     */
    protected function setDateTime($dateTime, string $property): OutboundStatusTimestamp
    {
        if ($dateTime === null) {
            return $this;
        }

        if (is_string($dateTime)) {
            $dateTime = (new DateTime($dateTime))->setTimezone(new DateTimeZone('UTC'));
        }

        $this->checkDate($dateTime);
        $this->{$property} = $dateTime;

        return $this;
    }

    /**
     * @return PropertyCollection
     */
    protected function loadProperties(): PropertyCollection
    {
        return new PropertyCollection([
            new PropertyInfo('pending', DateTime::class, null),
            new PropertyInfo('preparation', DateTime::class, null),
            new PropertyInfo('acknowledged', DateTime::class, null),
            new PropertyInfo('locked', DateTime::class, null),
            new PropertyInfo('pickprocess', DateTime::class, null),
            new PropertyInfo('shipped', DateTime::class, null),
            new PropertyInfo('partiallyShipped', DateTime::class, null),
            new PropertyInfo('canceled', DateTime::class, null),
            new PropertyInfo('partiallyCanceled', DateTime::class, null),
        ]);
    }
}
