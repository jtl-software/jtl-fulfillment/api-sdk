<?php
namespace Jtl\Fulfillment\Api\Sdk\Models\General\Stock;

use Jtl\Fulfillment\Api\Sdk\Models\DataModel;
use Izzle\Model\PropertyCollection;
use Izzle\Model\PropertyInfo;

/**
 * Class StockChangeId
 * @package Jtl\Fulfillment\Api\Sdk\Models\General\Stock
 */
class StockChangeId extends DataModel
{
    /**
     * @var string|null
     */
    protected $warehouseId;
    
    /**
     * @var string|null
     */
    protected $jfsku;
    
    /**
     * @var int|null
     */
    protected $stockVersion;
    
    /**
     * @return string|null
     */
    public function getWarehouseId(): ?string
    {
        return $this->warehouseId;
    }
    
    /**
     * @param string|null $warehouseId
     * @return StockChangeId
     */
    public function setWarehouseId(?string $warehouseId): StockChangeId
    {
        $this->warehouseId = $warehouseId;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getJfsku(): ?string
    {
        return $this->jfsku;
    }
    
    /**
     * @param string|null $jfsku
     * @return StockChangeId
     */
    public function setJfsku(?string $jfsku): StockChangeId
    {
        $this->jfsku = $jfsku;
        
        return $this;
    }
    
    /**
     * @return int|null
     */
    public function getStockVersion(): ?int
    {
        return $this->stockVersion;
    }
    
    /**
     * @param int|null $stockVersion
     * @return StockChangeId
     */
    public function setStockVersion(?int $stockVersion): StockChangeId
    {
        $this->stockVersion = $stockVersion;
        
        return $this;
    }
    
    /**
     * @return PropertyCollection
     */
    protected function loadProperties(): PropertyCollection
    {
        return new PropertyCollection([
            new PropertyInfo('warehouseId', 'string', null),
            new PropertyInfo('jfsku', 'string', null),
            new PropertyInfo('stockVersion', 'int', null)
        ]);
    }
}
