<?php
namespace Jtl\Fulfillment\Api\Sdk\Models\General\Stock;

use Jtl\Fulfillment\Api\Sdk\Models\DataModel;
use Izzle\Model\PropertyCollection;
use Izzle\Model\PropertyInfo;

/**
 * Class StockAnnouncedDetail
 * @package Jtl\Fulfillment\Api\Sdk\Models\General\Stock
 */
class StockAnnouncedDetail extends DataModel
{
    /**
     * @var string|null
     */
    protected $inboundId;

    /**
     * @var string|null
     */
    protected $inboundItemId;

    /**
     * @var float|null
     */
    protected $quantityAnnounced;

    /**
     * @return string|null
     */
    public function getInboundId(): ?string
    {
        return $this->inboundId;
    }

    /**
     * @param string|null $inboundId
     * @return StockAnnouncedDetail
     */
    public function setInboundId(?string $inboundId): StockAnnouncedDetail
    {
        $this->inboundId = $inboundId;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getInboundItemId(): ?string
    {
        return $this->inboundItemId;
    }

    /**
     * @param string|null $inboundItemId
     * @return StockAnnouncedDetail
     */
    public function setInboundItemId(?string $inboundItemId): StockAnnouncedDetail
    {
        $this->inboundItemId = $inboundItemId;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getQuantityAnnounced(): ?float
    {
        return $this->quantityAnnounced;
    }

    /**
     * @param float|null $quantityAnnounced
     * @return StockAnnouncedDetail
     */
    public function setQuantityAnnounced(?float $quantityAnnounced): StockAnnouncedDetail
    {
        $this->quantityAnnounced = $quantityAnnounced;

        return $this;
    }

    /**
     * @return PropertyCollection
     */
    protected function loadProperties(): PropertyCollection
    {
        return new PropertyCollection([
            new PropertyInfo('inboundId', 'string', null),
            new PropertyInfo('inboundItemId', 'string', null),
            new PropertyInfo('quantityAnnounced', 'float', null)
        ]);
    }
}
