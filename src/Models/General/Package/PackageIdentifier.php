<?php
namespace Jtl\Fulfillment\Api\Sdk\Models\General\Package;

use Jtl\Fulfillment\Api\Sdk\Models\DataModel;
use Izzle\Model\PropertyCollection;
use Izzle\Model\PropertyInfo;

/**
 * Class PackageIdentifier
 * @package Jtl\Fulfillment\Api\Sdk\Models\General\Package
 */
class PackageIdentifier extends DataModel
{
    /**
     * @var string|null
     */
    protected $value;
    
    /**
     * @var string|null
     */
    protected $identifierType;
    
    /**
     * @return string|null
     */
    public function getValue(): ?string
    {
        return $this->value;
    }
    
    /**
     * @param string|null $value
     * @return PackageIdentifier
     */
    public function setValue(?string $value): PackageIdentifier
    {
        $this->value = $value;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getIdentifierType(): ?string
    {
        return $this->identifierType;
    }
    
    /**
     * @param string|null $identifierType
     * @return PackageIdentifier
     */
    public function setIdentifierType(?string $identifierType): PackageIdentifier
    {
        $this->identifierType = $identifierType;
        
        return $this;
    }
    
    /**
     * @return PropertyCollection
     */
    protected function loadProperties(): PropertyCollection
    {
        return new PropertyCollection([
            new PropertyInfo('value', 'string', null),
            new PropertyInfo('identifierType', 'string', null)
        ]);
    }
}
