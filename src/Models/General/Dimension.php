<?php
namespace Jtl\Fulfillment\Api\Sdk\Models\General;

use Jtl\Fulfillment\Api\Sdk\Models\DataModel;
use Izzle\Model\PropertyCollection;
use Izzle\Model\PropertyInfo;

/**
 * Class Dimension
 * @package Jtl\Fulfillment\Api\Sdk\Models\General
 */
class Dimension extends DataModel
{
    /**
     * @var float|null
     */
    protected $width;
    
    /**
     * @var float|null
     */
    protected $length;
    
    /**
     * @var float|null
     */
    protected $height;
    
    /**
     * @return float|null
     */
    public function getWidth(): ?float
    {
        return $this->width;
    }
    
    /**
     * @param float|null $width
     * @return Dimension
     */
    public function setWidth(?float $width): Dimension
    {
        $this->width = $width;
        
        return $this;
    }
    
    /**
     * @return float|null
     */
    public function getLength(): ?float
    {
        return $this->length;
    }
    
    /**
     * @param float|null $length
     * @return Dimension
     */
    public function setLength(?float $length): Dimension
    {
        $this->length = $length;
        
        return $this;
    }
    
    /**
     * @return float|null
     */
    public function getHeight(): ?float
    {
        return $this->height;
    }
    
    /**
     * @param float|null $height
     * @return Dimension
     */
    public function setHeight(?float $height): Dimension
    {
        $this->height = $height;
        
        return $this;
    }
    
    /**
     * @return PropertyCollection
     */
    protected function loadProperties(): PropertyCollection
    {
        return new PropertyCollection([
            new PropertyInfo('width', 'float', null),
            new PropertyInfo('height', 'float', null),
            new PropertyInfo('length', 'float', null)
        ]);
    }
}
