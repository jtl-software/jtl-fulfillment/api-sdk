<?php
namespace Jtl\Fulfillment\Api\Sdk\Models\General;

use Izzle\Model\PropertyCollection;
use Izzle\Model\PropertyInfo;
use Jtl\Fulfillment\Api\Sdk\Models\DataModel;

class ChangeValueFloat extends DataModel
{
    /**
     * @var float|null
     */
    protected $value;

    /**
     * @var float|null
     */
    protected $previousValue;

    /**
     * @return float|null
     */
    public function getValue(): ?float
    {
        return $this->value;
    }

    /**
     * @param float|null $value
     * @return ChangeValueFloat
     */
    public function setValue(?float $value): ChangeValueFloat
    {
        $this->value = $value;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getPreviousValue(): ?float
    {
        return $this->previousValue;
    }

    /**
     * @param float|null $previousValue
     * @return ChangeValueFloat
     */
    public function setPreviousValue(?float $previousValue): ChangeValueFloat
    {
        $this->previousValue = $previousValue;
        return $this;
    }

    /**
     * @return PropertyCollection
     */
    protected function loadProperties(): PropertyCollection
    {
        return new PropertyCollection([
            new PropertyInfo('value', 'float', null),
            new PropertyInfo('previousValue', 'float', null),
        ]);
    }
}
