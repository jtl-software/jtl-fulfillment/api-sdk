<?php
namespace Jtl\Fulfillment\Api\Sdk\Models\General;

use Jtl\Fulfillment\Api\Sdk\Models\DataModel;
use Izzle\Model\PropertyCollection;
use Izzle\Model\PropertyInfo;

/**
 * Class User
 * @package Jtl\Fulfillment\Api\Sdk\Models\General
 */
class User extends DataModel
{
    public const ROLE_FULFILLER = 'Fulfiller';
    public const ROLE_MERCHANT = 'Merchant';
    public const ROLE_PORTAL = 'Portal';
    public const ROLE_ADMIN = 'Admin';
    public const ROLE_ACCOUNT = 'Account';
    public const ROLE_ANY = 'Any';
    
    /**
     * @var string[]
     */
    protected $roles = [];
    
    /**
     * @var string|null
     */
    protected $userId;
    
    /**
     * @var Address|null
     */
    protected $address;
    
    /**
     * @return string[]
     */
    public function getRoles(): array
    {
        return $this->roles;
    }
    
    /**
     * @param string[] $roles
     * @return User
     */
    public function setRoles(array $roles): User
    {
        $this->roles = $roles;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getUserId(): ?string
    {
        return $this->userId;
    }
    
    /**
     * @param string|null $userId
     * @return User
     */
    public function setUserId(?string $userId): User
    {
        $this->userId = $userId;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->getAddress() !== null ?
            $this->getAddress()->getEmail() : null;
    }
    
    /**
     * @return Address|null
     */
    public function getAddress(): ?Address
    {
        return $this->address;
    }
    
    /**
     * @param Address $address
     * @return User
     */
    public function setAddress(Address $address): User
    {
        $this->address = $address;
        
        return $this;
    }
    
    /**
     * @param string $name
     * @return mixed|string|null
     */
    public function __get($name)
    {
        if ($name === 'email') {
            return $this->getAddress() !== null ?
                $this->getAddress()->getEmail() : null;
        }
        
        return null;
    }
    
    /**
     * @return PropertyCollection
     */
    protected function loadProperties(): PropertyCollection
    {
        return new PropertyCollection([
            new PropertyInfo('roles', 'string', [], false, true),
            new PropertyInfo('userId', 'string', null),
            new PropertyInfo('address', Address::class, null, true)
        ]);
    }
}
