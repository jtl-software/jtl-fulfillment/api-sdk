<?php
namespace Jtl\Fulfillment\Api\Sdk\Models\General;

use Izzle\Model\PropertyCollection;
use Izzle\Model\PropertyInfo;
use Jtl\Fulfillment\Api\Sdk\Models\DataModel;

class ChangeValue extends DataModel
{
    /**
     * @var string|null
     */
    protected $value;

    /**
     * @var string|null
     */
    protected $previousValue;

    /**
     * @return string|null
     */
    public function getValue(): ?string
    {
        return $this->value;
    }

    /**
     * @param string|null $value
     * @return ChangeValue
     */
    public function setValue(?string $value): ChangeValue
    {
        $this->value = $value;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPreviousValue(): ?string
    {
        return $this->previousValue;
    }

    /**
     * @param string|null $previousValue
     * @return ChangeValue
     */
    public function setPreviousValue(?string $previousValue): ChangeValue
    {
        $this->previousValue = $previousValue;
        return $this;
    }

    /**
     * @return PropertyCollection
     */
    protected function loadProperties(): PropertyCollection
    {
        return new PropertyCollection([
            new PropertyInfo('value', 'string', null),
            new PropertyInfo('previousValue', 'string', null),
        ]);
    }
}
