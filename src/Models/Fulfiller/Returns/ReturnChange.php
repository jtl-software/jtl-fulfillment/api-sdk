<?php
namespace Jtl\Fulfillment\Api\Sdk\Models\Fulfiller\Returns;

use Izzle\Model\PropertyCollection;
use Izzle\Model\PropertyInfo;
use Jtl\Fulfillment\Api\Sdk\Models\General\ChangeValue;
use Jtl\Fulfillment\Api\Sdk\Models\General\Returns\ReturnChange as BaseReturnChange;

/**
 * Class ReturnChange
 * @package Jtl\Fulfillment\Api\Sdk\Models\Fulfiller\Returns
 */
class ReturnChange extends BaseReturnChange
{
    /**
     * @var ChangeValue|null
     */
    protected $merchantId;

    /**
     * @return ChangeValue|null
     */
    public function getMerchantId(): ?ChangeValue
    {
        return $this->merchantId;
    }

    /**
     * @param ChangeValue|null $merchantId
     * @return ReturnChange
     */
    public function setMerchantId(?ChangeValue $merchantId): ReturnChange
    {
        $this->merchantId = $merchantId;
        return $this;
    }

    /**
     * @return PropertyCollection
     */
    protected function loadProperties(): PropertyCollection
    {
        return parent::loadProperties()->addProperties([
            new PropertyInfo('merchantId', ChangeValue::class, [], true),
        ]);
    }
}
