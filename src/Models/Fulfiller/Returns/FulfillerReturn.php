<?php
namespace Jtl\Fulfillment\Api\Sdk\Models\Fulfiller\Returns;

use Izzle\Model\PropertyCollection;
use Izzle\Model\PropertyInfo;
use Jtl\Fulfillment\Api\Sdk\Models\General\Returns\BaseReturn;
use Jtl\Fulfillment\Api\Sdk\Models\Fulfiller\Returns\ReturnItem as FulfillerReturnItem;

/**
 * Class FulfillerReturn
 * @package Jtl\Fulfillment\Api\Sdk\Models\Fulfiller\Returns
 */
class FulfillerReturn extends BaseReturn
{
    /**
     * @var string|null
     */
    protected $merchantId;

    /**
     * @return string|null
     */
    public function getMerchantId(): ?string
    {
        return $this->merchantId;
    }

    /**
     * @param string|null $merchantId
     * @return FulfillerReturn
     */
    public function setMerchantId(?string $merchantId): FulfillerReturn
    {
        $this->merchantId = $merchantId;
        return $this;
    }

    /**
     * @return PropertyCollection
     */
    protected function loadProperties(): PropertyCollection
    {
        return parent::loadProperties()->addProperties([
            new PropertyInfo('merchantId', 'string', null),
            new PropertyInfo('items', FulfillerReturnItem::class, [], true, true),
        ]);
    }
}
