<?php
namespace Jtl\Fulfillment\Api\Sdk\Models\Fulfiller\Returns;

use Izzle\Model\PropertyCollection;
use Izzle\Model\PropertyInfo;
use Jtl\Fulfillment\Api\Sdk\Models\General\Returns\ReturnChangeShell as BaseReturnChangeShell;

/**
 * Class ReturnChangeShell
 * @package Jtl\Fulfillment\Api\Sdk\Models\Fulfiller\Returns
 */
class ReturnChangeShell extends BaseReturnChangeShell
{
    /**
     * @return PropertyCollection
     */
    protected function loadProperties(): PropertyCollection
    {
        return parent::loadProperties()->addProperties([
            new PropertyInfo('changes', ReturnChange::class, null, true),
        ]);
    }
}
