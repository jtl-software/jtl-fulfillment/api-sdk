<?php
namespace Jtl\Fulfillment\Api\Sdk\Models\Fulfiller\Stock;

use Izzle\Model\PropertyCollection;
use Izzle\Model\PropertyInfo;
use Jtl\Fulfillment\Api\Sdk\Models\General\Stock\StockChange as GeneralStockChange;

/**
 * Class StockChange
 * @package Jtl\Fulfillment\Api\Sdk\Models\Fulfiller\Stock
 */
class StockChange extends GeneralStockChange
{
    /**
     * @var StockInboundItem|null
     */
    protected $inboundItem;
    
    /**
     * @var StockOutboundItem|null
     */
    protected $outboundItem;
    
    /**
     * @return StockInboundItem|null
     */
    public function getInboundItem(): ?StockInboundItem
    {
        return $this->inboundItem;
    }
    
    /**
     * @param StockInboundItem $inboundItem
     * @return StockChange
     */
    public function setInboundItem(StockInboundItem $inboundItem): StockChange
    {
        $this->inboundItem = $inboundItem;
        
        return $this;
    }
    
    /**
     * @return StockOutboundItem|null
     */
    public function getOutboundItem(): ?StockOutboundItem
    {
        return $this->outboundItem;
    }
    
    /**
     * @param StockOutboundItem $outboundItem
     * @return StockChange
     */
    public function setOutboundItem(StockOutboundItem $outboundItem): StockChange
    {
        $this->outboundItem = $outboundItem;
        
        return $this;
    }
    
    /**
     * @return PropertyCollection
     */
    protected function loadProperties(): PropertyCollection
    {
        return parent::loadProperties()
            ->addProperties([
                new PropertyInfo('inboundItem', StockInboundItem::class, null, true),
                new PropertyInfo('outboundItem', StockOutboundItem::class, null, true)
            ]);
    }
}
