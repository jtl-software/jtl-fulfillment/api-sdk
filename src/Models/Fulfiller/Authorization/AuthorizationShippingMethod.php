<?php
namespace Jtl\Fulfillment\Api\Sdk\Models\Fulfiller\Authorization;

use DateTime;
use DateTimeZone;
use Exception;
use function is_string;
use Jtl\Fulfillment\Api\Sdk\Models\DataModel;
use Izzle\Model\PropertyCollection;
use Izzle\Model\PropertyInfo;

/**
 * Class AuthorizationShippingMethod
 * @package Jtl\Fulfillment\Api\Sdk\Models\Fulfiller\Authorization
 */
class AuthorizationShippingMethod extends DataModel
{
    /**
     * @var string|null
     */
    protected $shippingMethodId;
    
    /**
     * @var DateTime|null
     */
    protected $createdAt;
    
    /**
     * @return string|null
     */
    public function getShippingMethodId(): ?string
    {
        return $this->shippingMethodId;
    }
    
    /**
     * @param string|null $shippingMethodId
     * @return AuthorizationShippingMethod
     */
    public function setShippingMethodId(?string $shippingMethodId): AuthorizationShippingMethod
    {
        $this->shippingMethodId = $shippingMethodId;
        
        return $this;
    }
    
    /**
     * @return DateTime|null
     */
    public function getCreatedAt(): ?DateTime
    {
        return $this->createdAt;
    }
    
    /**
     * @param DateTime|string|null $createdAt
     * @return AuthorizationShippingMethod
     * @throws Exception
     */
    public function setCreatedAt($createdAt): AuthorizationShippingMethod
    {
        if ($createdAt === null) {
            return $this;
        }
        
        if (is_string($createdAt)) {
            $createdAt = (new DateTime($createdAt))->setTimezone(new DateTimeZone('UTC'));
        }
        
        $this->checkDate($createdAt);
        $this->createdAt = $createdAt;
        
        return $this;
    }
    
    /**
     * @return PropertyCollection
     */
    protected function loadProperties(): PropertyCollection
    {
        return (new PropertyCollection())->setProperties([
            new PropertyInfo('shippingMethodId', 'string', null),
            new PropertyInfo('createdAt', DateTime::class, null)
        ]);
    }
}
