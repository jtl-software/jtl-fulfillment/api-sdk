<?php
namespace Jtl\Fulfillment\Api\Sdk\Models\Fulfiller\Inbound;

use Jtl\Fulfillment\Api\Sdk\Models\DataModel;
use Izzle\Model\PropertyCollection;
use Izzle\Model\PropertyInfo;
use Jtl\Fulfillment\Api\Sdk\Models\General\Inbound\Inbound;

/**
 * Class IncomingGood
 * @package Jtl\Fulfillment\Api\Sdk\Models\Fulfiller\Inbound
 */
class IncomingGood extends DataModel
{
    /**
     * @var string|null
     */
    protected $note;
    
    /**
     * @var IncomingGoodItem[]
     */
    protected $items = [];
    
    /**
     * @return string|null
     */
    public function getNote(): ?string
    {
        return $this->note;
    }
    
    /**
     * @param string|null $note
     * @return IncomingGood
     */
    public function setNote(?string $note): IncomingGood
    {
        $this->note = $note;
        
        return $this;
    }
    
    /**
     * @return IncomingGoodItem[]
     */
    public function getItems(): array
    {
        return $this->items;
    }
    
    /**
     * @param IncomingGoodItem[] $items
     * @return IncomingGood
     */
    public function setItems(array $items): IncomingGood
    {
        $this->items = $items;
        
        return $this;
    }
    
    /**
     * @param IncomingGoodItem $item
     * @param string|null $key
     * @return IncomingGood
     */
    public function addItem(IncomingGoodItem $item, string $key = null): IncomingGood
    {
        if ($key === null) {
            $this->items[] = $item;
        } else {
            $this->items[$key] = $item;
        }
        
        return $this;
    }
    
    /**
     * @return PropertyCollection
     */
    protected function loadProperties(): PropertyCollection
    {
        return new PropertyCollection([
            new PropertyInfo('note', 'string', null),
            new PropertyInfo('items', IncomingGoodItem::class, [], true, true)
        ]);
    }
}
