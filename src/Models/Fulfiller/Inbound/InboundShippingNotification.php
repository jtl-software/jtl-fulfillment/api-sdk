<?php
namespace Jtl\Fulfillment\Api\Sdk\Models\Fulfiller\Inbound;

use Izzle\Model\PropertyCollection;
use Izzle\Model\PropertyInfo;
use Jtl\Fulfillment\Api\Sdk\Models\General\Inbound\InboundShippingNotification as GeneralInboundShippingNotification;

class InboundShippingNotification extends GeneralInboundShippingNotification
{
    /**
     * @var string|null
     */
    protected $merchantId;
    
    /**
     * @return string|null
     */
    public function getMerchantId(): ?string
    {
        return $this->merchantId;
    }
    
    /**
     * @param string|null $merchantId
     * @return InboundShippingNotification
     */
    public function setMerchantId(?string $merchantId): InboundShippingNotification
    {
        $this->merchantId = $merchantId;
        
        return $this;
    }
    
    /**
     * @return PropertyCollection
     */
    protected function loadProperties(): PropertyCollection
    {
        return parent::loadProperties()->addProperties([
            new PropertyInfo('merchantId', 'string', null)
        ]);
    }
}
