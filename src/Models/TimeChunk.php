<?php
namespace Jtl\Fulfillment\Api\Sdk\Models;

use DateTime;
use DateTimeZone;
use Exception;
use function is_string;
use Jtl\Fulfillment\Api\Sdk\Models\DataModel;
use Izzle\Model\PropertyCollection;
use Izzle\Model\PropertyInfo;

class TimeChunk extends DataModel
{
    /**
     * @var Model[]
     */
    protected $items = [];

    /**
     * @var string
     */
    protected $nextChunkUrl = '';

    /**
     * @var DateTime|null
     */
    protected $date;

    /**
     * @var bool
     */
    protected $moreDataAvailable = false;

    /**
     * @return Model[]
     */
    public function getItems(): array
    {
        return $this->items;
    }

    /**
     * @param Model[] $items
     * @return TimeChunk
     */
    public function setItems(array $items): TimeChunk
    {
        $this->items = $items;

        return $this;
    }

    /**
     * @return string
     */
    public function getNextChunkUrl(): string
    {
        return $this->nextChunkUrl;
    }

    /**
     * @param string $nextChunkUrl
     * @return TimeChunk
     */
    public function setNextChunkUrl(string $nextChunkUrl): TimeChunk
    {
        $this->nextChunkUrl = $nextChunkUrl;

        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getDate(): ?DateTime
    {
        return $this->date;
    }

    /**
     * @param DateTime|null|string $date
     * @return TimeChunk
     * @throws Exception
     */
    public function setDate($date): TimeChunk
    {
        if ($date === null) {
            return $this;
        }

        if (is_string($date)) {
            $date = (new DateTime($date))->setTimezone(new DateTimeZone('UTC'));
        }

        $this->checkDate($date);
        $this->date = $date;

        return $this;
    }

    /**
     * @return bool
     */
    public function isMoreDataAvailable(): bool
    {
        return $this->moreDataAvailable;
    }

    /**
     * @return bool
     */
    public function getMoreDataAvailable(): bool
    {
        return $this->moreDataAvailable;
    }

    /**
     * @param bool $moreDataAvailable
     * @return TimeChunk
     */
    public function setMoreDataAvailable(bool $moreDataAvailable): TimeChunk
    {
        $this->moreDataAvailable = $moreDataAvailable;
        return $this;
    }

    /**
     * @return PropertyCollection
     */
    protected function loadProperties(): PropertyCollection
    {
        return new PropertyCollection([
            new PropertyInfo('items', 'mixed', []),
            new PropertyInfo('date', DateTime::class, null),
            new PropertyInfo('nextChunkUrl'),
            new PropertyInfo('moreDataAvailable', 'bool', false),
        ]);
    }
}
