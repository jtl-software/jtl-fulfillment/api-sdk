<?php
namespace Jtl\Fulfillment\Api\Sdk\Models;

use Closure;
use DateTime;
use InvalidArgumentException;
use Jtl\Fulfillment\Api\Sdk\Models\DataModel;
use Izzle\Model\PropertyCollection;
use Izzle\Model\PropertyInfo;
use Jtl\Fulfillment\Api\Sdk\Query\Builder;
use Jtl\Fulfillment\Api\Sdk\Query\Grammars\FilterGrammar;
use Jtl\Fulfillment\Api\Sdk\Query\Grammars\ODataGrammar;

/**
 * Class Query
 * @package Jtl\Fulfillment\Api\Sdk\Models
 */
class Query extends DataModel
{
    // Directions
    public const DIR_ASC = 'asc';
    public const DIR_DESC = 'desc';
    
    /**
     * @var bool - Serializing in OData format
     */
    public static $oDataSerialization = false;
    
    /**
     * @var string[]
     */
    protected $directions = [
        self::DIR_ASC,
        self::DIR_DESC,
    ];
    
    /**
     * @var int
     */
    protected $limit = 100;

    /**
     * @var int
     */
    protected $offset = 0;
    
    /**
     * @var int
     */
    protected $page = 1;

    /**
     * @var string
     */
    protected $order = '';

    /**
     * @var string
     */
    protected $dir = self::DIR_ASC;
    
    /**
     * @var DateTime
     */
    protected $fromDate;
    
    /**
     * @var DateTime
     */
    protected $toDate;
    
    /**
     * @var Builder
     */
    protected $builder;
    
    /**
     * @var QueryParam[]
     */
    protected $params = [];
    
    /**
     * Query constructor.
     * @param array|null $data
     */
    public function __construct(?array $data = null)
    {
        parent::__construct($data);
        
        $this->builder = new Builder();
    }
    
    /**
     * @return int
     */
    public function getLimit(): int
    {
        return $this->limit;
    }

    /**
     * @param int $limit
     * @return self
     */
    public function setLimit(int $limit): self
    {
        $this->limit = $limit;

        return $this;
    }

    /**
     * @return int
     */
    public function getOffset(): int
    {
        return $this->offset;
    }

    /**
     * @param int $offset
     * @return self
     */
    public function setOffset(int $offset): self
    {
        $this->offset = $offset;
        if ($this->limit > 0) {
            $this->page = $this->offset / $this->limit + 1;
        }

        return $this;
    }
    
    /**
     * @return int
     */
    public function getPage(): int
    {
        return $this->page;
    }
    
    /**
     * @param int $page
     * @return Query
     */
    public function setPage(int $page): Query
    {
        $this->page = $page;
        if ($this->limit > 0) {
            $this->offset = ($this->page - 1) * $this->limit;
        }
        
        return $this;
    }

    /**
     * @return string
     */
    public function getOrder(): string
    {
        return $this->order;
    }

    /**
     * @param string $order
     * @return self
     */
    public function setOrder(string $order): self
    {
        $this->order = $order;

        return $this;
    }

    /**
     * @return string
     */
    public function getDir(): string
    {
        return $this->dir;
    }

    /**
     * @param string $dir
     * @return self
     * @throws InvalidArgumentException
     */
    public function setDir(string $dir): self
    {
        $this->checkDirection($dir);
        
        $this->dir = $dir;

        return $this;
    }
    
    /**
     * @return DateTime|null
     */
    public function getFromDate(): ?DateTime
    {
        return $this->fromDate;
    }
    
    /**
     * @param DateTime $fromDate
     * @return Query
     */
    public function setFromDate(DateTime $fromDate): Query
    {
        $this->checkDate($fromDate);
        
        $this->fromDate = $fromDate;
        
        return $this;
    }
    
    /**
     * @return DateTime|null
     */
    public function getToDate(): ?DateTime
    {
        return $this->toDate;
    }
    
    /**
     * @param DateTime $toDate
     * @return Query
     */
    public function setToDate(DateTime $toDate): Query
    {
        $this->checkDate($toDate);
        
        $this->toDate = $toDate;
        
        return $this;
    }
    
    /**
     * @return QueryParam[]
     */
    public function getParams(): array
    {
        return $this->params;
    }
    
    /**
     * @param QueryParam[] $params
     * @return Query
     * @throws InvalidArgumentException
     */
    public function setParams(array $params): Query
    {
        foreach ($params as $param) {
            if (!($param instanceof QueryParam)) {
                throw new InvalidArgumentException(sprintf(
                    'Every entry must be instance of %s',
                    QueryParam::class
                ));
            }
            
            $this->params[$param->getKey()] = $param;
        }
        
        return $this;
    }
    
    /**
     * @param QueryParam $param
     * @return Query
     */
    public function addParam(QueryParam $param): Query
    {
        $this->params[$param->getKey()] = $param;
        
        return $this;
    }
    
    /**
     * @param string $key
     * @return QueryParam|null
     */
    public function getParam(string $key): ?QueryParam
    {
        if (!empty($this->params[$key])) {
            return $this->params[$key];
        }
        
        return null;
    }
    
    /**
     * @param QueryParam $param
     * @return Query
     */
    public function removeParam(QueryParam $param): Query
    {
        if (isset($this->params[$param->getKey()])) {
            unset($this->params[$param->getKey()]);
        }
    
        return $this;
    }
    
    /**
     * @param string $key
     * @return Query
     */
    public function removeParamByKey(string $key): Query
    {
        if (isset($this->params[$key])) {
            unset($this->params[$key]);
        }
        
        return $this;
    }
    
    /**
     * @return string
     */
    public function buildQueryString(): string
    {
        $str = '';
        
        /** @var QueryParam $param */
        $i = 0;
        foreach ($this->params as $param) {
            $str .= sprintf('%s%s', $i > 0 ? '&' : '?', $param->build());
            $i++;
        }
        
        return $str;
    }
    
    /**
     * @param string|array|Closure
     * @param string|null $operator
     * @param mixed|null $value
     * @return self
     */
    public function where($column, string $operator = null, $value = null): self
    {
        $this->builder->where($column, $operator, $value);
        
        return $this;
    }
    
    /**
     * @param string|array|Closure
     * @param string|null $operator
     * @param mixed|null $value
     * @return self
     */
    public function orWhere($column, string $operator = null, $value = null): self
    {
        $this->builder->orWhere($column, $operator, $value);
    
        return $this;
    }
    
    /**
     * @param $column
     * @param string $boolean
     * @param bool $not
     * @return Query
     */
    public function whereNull($column, $boolean = 'and', $not = false): self
    {
        $this->builder->whereNull($column, $boolean, $not);
        
        return $this;
    }
    
    /**
     * @param $column
     * @param mixed|null $value
     * @return Query
     */
    public function whereContains($column, $value = null): self
    {
        $this->builder->whereContains($column, $value);
        
        return $this;
    }
    
    /**
     * @param $column
     * @param mixed|null $value
     * @return Query
     */
    public function orWhereContains($column, $value = null): self
    {
        $this->builder->orWhereContains($column, $value);
        
        return $this;
    }
    
    /**
     * @param $column
     * @param mixed|null $value
     * @return Query
     */
    public function whereStartsWith($column, $value = null): self
    {
        $this->builder->whereStartsWith($column, $value);
    
        return $this;
    }
    
    /**
     * @param $column
     * @param mixed|null $value
     * @return Query
     */
    public function orWhereStartsWith($column, $value = null): self
    {
        $this->builder->orWhereStartsWith($column, $value);
        
        return $this;
    }
    
    /**
     * @param $column
     * @param array $values
     * @return Query
     */
    public function whereIn($column, array $values): self
    {
        $this->builder->whereIn($column, $values);
        
        return $this;
    }
    
    /**
     * @param $column
     * @param array $values
     * @return Query
     */
    public function orWhereIn($column, array $values): self
    {
        $this->builder->orWhereIn($column, $values);
    
        return $this;
    }
    
    /**
     * @return Query
     */
    public function clean(): self
    {
        $this->builder->clean();
        
        return $this;
    }
    
    /**
     * @param string|array $expand
     * @throws InvalidArgumentException
     * @return $this
     */
    public function with($expand): self
    {
        $this->builder->with($expand);
        
        return $this;
    }
    
    /**
     * @param string $child
     * @param string|null $column
     * @param string|null $operator
     * @param mixed|null $value
     * @param string $boolean
     * @return Query
     */
    public function whereSub(string $child, string $column = null, string $operator = null, $value = null, string $boolean = 'and'): self
    {
        $this->builder->whereSub($child, $column, $operator, $value, $boolean);
        
        return $this;
    }
    
    /**
     * @param string $child
     * @param string|null $column
     * @param string|null $operator
     * @param mixed|null $value
     * @return Query
     */
    public function orWhereSub(string $child, string $column = null, string $operator = null, $value = null): self
    {
        $this->whereSub($child, $column, $operator, $value, 'or');
        
        return $this;
    }
    
    /**
     * @return array
     */
    public function toArray(): array
    {
        if (self::$oDataSerialization) {
            return $this->toODataArray();
        }
        
        $data = [
            'limit' => $this->limit,
            'offset' => $this->offset,
            'page' => $this->page,
        ];
        
        if (!empty($this->order)) {
            $data['order'] = $this->order;
        }
    
        if (!empty($this->dir)) {
            $data['dir'] = $this->dir;
        }
    
        if ($this->fromDate !== null) {
            $data['fromDate'] = $this->fromDate->format(self::$serializedDateTimeFormat);
        }
    
        if ($this->toDate !== null) {
            $data['toDate'] = $this->toDate->format(self::$serializedDateTimeFormat);
        }
    
        $filter = (new FilterGrammar())->compileWheresToArray($this->builder);
        
        if (count($filter) > 0) {
            $data['filter'] = $filter;
        }
    
        if (count($this->params) > 0) {
            /** @var QueryParam $param */
            foreach ($this->getParams() as $key => $param) {
                $data[$key] = $param->getValue();
            }
        }
        
        return $data;
    }
    
    /**
     * @return array
     */
    private function toODataArray(): array
    {
        $data = [
            '$top' => $this->limit,
            '$skip' => $this->offset,
        ];
    
        if (!empty($this->order)) {
            $data['$orderBy'] = sprintf('%s %s', $this->order, $this->dir);
        }
    
        $grammar = new ODataGrammar();
        $filter = $grammar->compileWheres($this->builder);
        
        $str_replace_first = static function ($from, $to, $content) {
            $from = '/' . preg_quote($from, '/') . '/';
    
            return preg_replace($from, $to, $content, 1);
        };
    
        if (!empty($filter)) {
            $data['$filter'] = $str_replace_first('$filter=', '', $filter);
        }
        
        $expand = $grammar->compileExpands($this->builder);
        
        if (!empty($expand)) {
            $data['$expand'] = $str_replace_first('$expand=', '', $expand);
        }
    
        return $data;
    }
    
    /**
     * @param string $direction
     * @throws InvalidArgumentException
     */
    private function checkDirection(string $direction): void
    {
        if (!in_array($direction, $this->directions)) {
            throw new InvalidArgumentException(sprintf('Direction %s is not supported', $direction));
        }
    }
    
    /**
     * @return PropertyCollection
     */
    protected function loadProperties(): PropertyCollection
    {
        return (new PropertyCollection())->setProperties([
            new PropertyInfo('limit', 'int', 100),
            new PropertyInfo('offset', 'int', 0),
            new PropertyInfo('page', 'int', 1),
            new PropertyInfo('order'),
            new PropertyInfo('dir'),
            new PropertyInfo('fromDate', DateTime::class, null),
            new PropertyInfo('toDate', DateTime::class, null),
            new PropertyInfo('params', QueryParam::class, [], true, true)
        ]);
    }
}
