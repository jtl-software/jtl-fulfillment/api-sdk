<?php
namespace Jtl\Fulfillment\Api\Sdk\Models\Merchant\Outbound;

use Izzle\Model\PropertyCollection;
use Izzle\Model\PropertyInfo;
use Jtl\Fulfillment\Api\Sdk\Models\General\Outbound\OutboundShippingNotification as GeneralOutboundShippingNotification;

/**
 * Class OutboundShippingNotification
 * @package Jtl\Fulfillment\Api\Sdk\Models\Merchant\Outbound
 */
class OutboundShippingNotification extends GeneralOutboundShippingNotification
{
    /**
     * @var string|null
     */
    protected $merchantOutboundNumber;
    
    /**
     * @var string|null
     */
    protected $fulfillerId;
    
    /**
     * @return string|null
     */
    public function getMerchantOutboundNumber(): ?string
    {
        return $this->merchantOutboundNumber;
    }
    
    /**
     * @param string|null $merchantOutboundNumber
     * @return OutboundShippingNotification
     */
    public function setMerchantOutboundNumber(?string $merchantOutboundNumber): OutboundShippingNotification
    {
        $this->merchantOutboundNumber = $merchantOutboundNumber;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getFulfillerId(): ?string
    {
        return $this->fulfillerId;
    }
    
    /**
     * @param string|null $fulfillerId
     * @return OutboundShippingNotification
     */
    public function setFulfillerId(?string $fulfillerId): OutboundShippingNotification
    {
        $this->fulfillerId = $fulfillerId;
        
        return $this;
    }
    
    /**
     * @return PropertyCollection
     */
    protected function loadProperties(): PropertyCollection
    {
        return parent::loadProperties()->addProperties([
            new PropertyInfo('merchantOutboundNumber', 'string', null),
            new PropertyInfo('fulfillerId', 'string', null),
        ]);
    }
}
