<?php
namespace Jtl\Fulfillment\Api\Sdk\Models\Merchant\Outbound;

use Izzle\Model\PropertyCollection;
use Izzle\Model\PropertyInfo;
use Jtl\Fulfillment\Api\Sdk\Models\General\Outbound\Outbound as GeneralOutbound;
use Jtl\Fulfillment\Api\Sdk\Models\Merchant\Returns\MerchantReturn;

/**
 * Class Outbound
 * @package Jtl\Fulfillment\Api\Sdk\Models\Merchant\Outbound
 */
class Outbound extends GeneralOutbound
{
    /**
     * @var string|null
     */
    protected $fulfillerId;
    
    /**
     * @var OutboundShippingLabelProviderData|null
     */
    protected $shippingLabelProviderData;
    
    /**
     * @return string|null
     */
    public function getFulfillerId(): ?string
    {
        return $this->fulfillerId;
    }
    
    /**
     * @param string|null $fulfillerId
     * @return Outbound
     */
    public function setFulfillerId(?string $fulfillerId): Outbound
    {
        $this->fulfillerId = $fulfillerId;
        
        return $this;
    }
    
    /**
     * @return OutboundShippingLabelProviderData|null
     */
    public function getShippingLabelProviderData(): ?OutboundShippingLabelProviderData
    {
        return $this->shippingLabelProviderData;
    }
    
    /**
     * @param OutboundShippingLabelProviderData|null $shippingLabelProviderData
     * @return Outbound
     */
    public function setShippingLabelProviderData(?OutboundShippingLabelProviderData $shippingLabelProviderData
    ): Outbound {
        $this->shippingLabelProviderData = $shippingLabelProviderData;
        
        return $this;
    }
    
    /**
     * @return PropertyCollection
     */
    protected function loadProperties(): PropertyCollection
    {
        return parent::loadProperties()->addProperties([
            new PropertyInfo('fulfillerId', 'string', null),
            new PropertyInfo('shippingLabelProviderData', OutboundShippingLabelProviderData::class, null, true),
            new PropertyInfo('relatedReturns', MerchantReturn::class, [], true, true),
        ]);
    }
}
