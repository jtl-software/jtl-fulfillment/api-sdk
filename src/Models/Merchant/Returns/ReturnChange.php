<?php
namespace Jtl\Fulfillment\Api\Sdk\Models\Merchant\Returns;

use Izzle\Model\PropertyCollection;
use Izzle\Model\PropertyInfo;
use Jtl\Fulfillment\Api\Sdk\Models\General\ChangeValue;
use Jtl\Fulfillment\Api\Sdk\Models\General\Returns\ReturnChange as BaseReturnChange;

/**
 * Class ReturnChange
 * @package Jtl\Fulfillment\Api\Sdk\Models\Merchant\Returns
 */
class ReturnChange extends BaseReturnChange
{
    /**
     * @var ChangeValue|null
     */
    protected $fulfillerId;

    /**
     * @return ChangeValue|null
     */
    public function getFulfillerId(): ?ChangeValue
    {
        return $this->fulfillerId;
    }

    /**
     * @param ChangeValue|null $fulfillerId
     * @return ReturnChange
     */
    public function setFulfillerId(?ChangeValue $fulfillerId): ReturnChange
    {
        $this->fulfillerId = $fulfillerId;
        return $this;
    }

    /**
     * @return PropertyCollection
     */
    protected function loadProperties(): PropertyCollection
    {
        return parent::loadProperties()->addProperties([
            new PropertyInfo('fulfillerId', ChangeValue::class, null, true),
        ]);
    }
}
